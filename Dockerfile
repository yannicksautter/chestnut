# Create deps image
FROM python:3.10 as python-deps
RUN apt-get update
RUN pip install pipenv
COPY Pipfile .
COPY Pipfile.lock .
# mabe add --system and ignore --ignore-pipfile
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

# Create new image from deps image
FROM python-deps as runtime
RUN apt-get install vim -y

RUN apt-get install graphviz -y

COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"
WORKDIR /code

COPY ./web_service .

RUN mkdir "/data"
ENV MEDIA_ROOT="/data"
ENV DATABASE_PATH="/data/db.sqlite3"
RUN python manage.py migrate
RUN mkdocs build
VOLUME ["/data"]

HEALTHCHECK CMD curl --fail http://localhost:80 || exit 1   

ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:80"]

