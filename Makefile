REMOTE_REGISTRY ?= ysautter
TAG ?= chestnut

run:
	docker-compose -f docker-compose.common.yml -f docker-compose.dev.yml up  --force-recreate --build

prod_run:
	docker-compose up  --force-recreate --build

push:
	docker push "$(REMOTE_REGISTRY)/$(TAG)"

build:
	docker build . -t "$(REMOTE_REGISTRY)/$(TAG)"

