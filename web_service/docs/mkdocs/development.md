# Development
The git repository resides in the [GitLab instance of RWTH](https://git.rwth-aachen.de/yannicksautter/chestnut).

## Requirements
- [pipenv](https://pipenv.pypa.io/en/latest/)
- [docker](https://docs.docker.com/get-docker/)
 
 We use pipenv for version control and pinning dependencies. 
## Getting Started
- For local development, use:
    - `pipenv install` to setup a virtual python environment (the environment may be entered using `pipenv shell`)
    - `pipenv run serve` to run the server
- For dockerized development run either: 
    - `docker-compose -f docker-compose.common.yml -f docker-compose.dev.yml up  --force-recreate --build`
    - `make run`
    - `pipenv run run`

After following any of the two approaches to get an instance running, one can start development
in the `web_service` folder. The instance watches for file changes and restarts automatically.  

!!! note
    In case of docker, the web service folder is mounted directly into the running container.
 
## Building the Manual
The manual is written using Markdown and produced by mkdocs.
For full documentation visit [mkdocs.org](https://www.mkdocs.org).
<!--I recommend using [mkdocs](https://github.com/mkdocs/mkdocs) which is written in python.-->
<!--`mkdocs build` or `mkdocs serve`-->

### Commands

* `mkdocs -h` - Print help message and exit.
* `mkdocs serve -f web_service/mkdocs.yml` - Start the live-reloading docs server.
* `mkdocs build -f web_service/mkdocs.yml` - Build the documentation site.

## Resources
- [PM4PY](https://pm4py.fit.fraunhofer.de/)
- [Django](https://www.djangoproject.com/)
- [Bootstrap](https://getbootstrap.com/)


## Software Requirement Specification

The SRS can be found [here](https://de.overleaf.com/project/6352e2b4ee7871ffad7a6905).

## Scrum Board

The Scrum Board can be found [here](https://trello.com/b/wYMXjTjD/chestnut-scrum-board).
