<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">

# Features
On this page, you can get an overview of all the available features and their technical details. For example-walkthroughs and use cases, check out the [Use Cases](/use-cases) page. Brief descriptions of each feature's functionality can also
be found within the application when the user hovers their mouse over the small tooltip helper icons <i class="fa fa-info-circle"></i>.

## Table of Contents
1. [Process Model Discovery](#process-model-discovery)
2. [Process Model](#process-model)
3. [Filters](#filters)
4. [Enhancement](#enhancement)

## Process Model Discovery
After the user selects an object-centric event log (OCEL) to import, in the next step, they need to select a process
model discovery algorithm. This algorithm is then used to discover an object-centric Petri net (OCPN) that can be used
to gain a better understanding of the process data at hand.

The following steps give a high-level overview of the technical details of this object-centric discovery process:

 1. Given an OCEL, flatten it so that we have a resulting standard case-centric log for each of the mentioned object types
 2. For each of the resulting standard logs, we can now apply standard case-centric discovery algorithms to discover 
classical Petri net process models. The currently, by us, supported discovery algorithms are:
    - Inductive Miner
    - Inductive Miner Infrequent
    - Inductive Miner Directly Follows
    - Alpha Miner Classic
    - Alpha Miner Plus
    - Heuristic Miner
    - Heuristic Miner Plus Plus
 3. Combine and merge the discovered classical Petri nets to an object-centric Petri net. In basic terms, this is done 
by assigning object types to places and arcs within each respective Petri net and then merging them based on shared 
transition names. Furthermore, it is possible to assign certain transitions to be variable which means they can consume
and produce multiple tokens from a given object type's place.

For an in-depth walkthrough of object-centric Petri nets and their discovery, read the paper 
["Discovering Object-Centric Petri Nets by Aalst, Berti"](http://www.padsweb.rwth-aachen.de/wvdaalst/publications/p1163.pdf).

## Process Model

![Process Model](res/img/processmodel.png)

The discovered object-centric Petri net process model is displayed as an interactive graph where the round nodes 
represent places and the rectangles represent transitions of the Petri net. Arcs and transitions belonging to different 
object types are encoded using different colors. Furthermore, transitions are labeled with the following annotations:

- EF: Event frequency refers to the number of events related to a given transition
- OF: Object frequency refers to the number of objects related to a given transition

### Decision Points

![Decision Points](res/img/decisionpoints.png)

Decision points can automatically be marked for suggestions on which areas of the Petri net might
be interesting to analyze. On a technical level, this is done by flattening the OCPN and computing
decision points for each object type separately. Decision points are computed by finding those
    transitions that have multiple outgoing arcs to different transitions. To trigger decision points use the **Toggle Decision Points** button: 
<img src="../res/img/decision_points_btn.png" style="width: 150px"/>
!!! note
    The suggested
    decision points might in some cases over-approximate the actually interesting decision points in the OCPN
    but this is where user discretion is advised.

## Filters

![Filters](res/img/filters.png)

Above the interactive OCPN graph, various filter options can be found. These allow for the filtering out of objects
(and thus indirectly events) from the OCEL. The filtered-down version of the OCEL can be used to discover interesting 
patterns present in the filtered subset of the log. 

### Fitness Filter
The fitness filter allows us to replay the object-centric event log on the discovered object-centric Petri net and 
filter out objects that do not meet a specified fitness requirement. For this, we replay each object on a flattened 
version of its object type’s Petri net and calculate the corresponding trace fitness. If this is below the passed 
threshold, the object is filtered out. In case all objects related to an event are removed, the event will be removed as 
well.

#### Parameters:
 - Fitness threshold: Objects that correspond to traces with a fitness below this value are filtered out.
 - Replay method: The user can choose which replay methodology should be used to compute the trace fitness. The
supported options are alignment-based and token-based replay. 
!!! note
    Alignment-based replay is only possible for sound
    workflow nets.


### Activity Filter
The activity filter allows us to filter out objects that are not involved in the execution of events with the selected 
activities. This can be particularly useful in case a user is interested in objects that go through certain
transitions/activities or even paths.  

#### Parameters:
 - Set of Activities: Objects with traces not mentioning these activities are filtered out. To select activities, press the **Select Tranitions** button, which toggles the selection mode, which is indicated by the buttons orange appearance: 
<img src="../res/img/filter_activities_btn.png" style="width: 120px"/>. By pressing the transitions of interest the form field is updated accordingly.
 - Use All Activities: If this is not selected, the activity filter requires the object traces to visit at least one of 
the specified activities. If, on the other hand, it is selected, all the specified activities need to be visited.


### Time-Frame Filter
This filter can be used to filter out events with timestamps outside of the passed time frame. This can be useful in 
case a user wants to analyze object patterns during a certain time period (e.g. time of year, special sales periods, etc.)

#### Parameters:
 - Start Date
 - End Date

## Enhancement
The enhancement feature deals with discovering what the object attribute data looks like at a given transition in the 
object-centric Petri net. This can be very useful to analyze what differentiates objects that visit different activities
and paths in the model. For an overview of some further use cases, check out the "Use Cases" page.

On a more technical level, the following steps outline the inner workings of the enhancement:

 1. User selects a transition of the OCPN that should be analyzed.
 2. User selects desired object types, attributes, and other required parameters that are of interest at the 
previously selected transition.
 3. The current working OCEL (i.e. the OCEL with all the globally selected filters applied to it) is filtered down to 
only those events that are related to the transition selected in step 1. 
 4. Once the OCEL has been reduced to only the relevant events, a table is created where for each event in the OCEL, a
row is added for each object (of the types specified in 2.) related to that event. Note that for type-level patterns, we 
aggregate the data belonging to the same object type before adding it as a row to the table.
 5. Now that we have a table containing the object attributes for each object mentioned in the relevant events, we can
apply pattern mining to the table. Furthermore, we can create boxplots for the numerical attributes mentioned in the 
table. 


### Output Patterns
The output generated from the enhancement is two-fold. On the one hand, patterns are displayed as boxplots that can be 
used to get an overview of what the given object data and its distribution looks like. On the other hand, there are
frequent item sets computed with the help of the FPGrowth pattern mining algorithm. 

![Patterns](res/img/patterns.png)

#### Pattern Mining and Frequent Itemsets
As mentioned above, the enhancement deploys the FPGrowth pattern mining algorithm for the computation of frequent 
item sets. For a step-by-step guide on FPGrowth, check out 
[this tutorial](https://www.softwaretestinghelp.com/fp-growth-algorithm-data-mining/).

Frequent item sets can be used to find common object attributes within the data. Some example applications of this are 
finding certain vendors or delivery services that are particularly common among especially wanted or unwanted activities 
(e.g. failed/successful deliveries, activities indicative for rework, and other areas with potential for efficiency gains).

!!! note
    Frequent item sets require categorical data which is why binning is applied to numerical attributes in order
    to discretize them. More on the input parameters can be read in the "Input Parameters" section.

#### Boxplots
Boxplots show the distribution of the numerical attribute values. They consist of 4 lines: minimum, first quartile, 
median, third quartile, and maximum in this order from bottom to top.

#### Type-Level Patterns 
A type-level pattern is derived by aggregating object attribute values of all objects passing through a transition at 
the same time. For example, consider 4 items related to an object-centric event with the “create package” activity name. 
In this case, a type-level pattern could, for instance, give you information on the sum of the weight of all items 
related to one execution of the transition labeled with “create package”. All attribute values of each object attribute 
related to an event are summed up for the aggregation. Due to this, only numerical attributes are considered here.

#### Frequency boxplot
The frequency boxplot depicts the distribution of the number of objects related to an execution of the chosen 
transition. For example, in case there always are exactly 4 items related to a “create package” event, this will be 
visible in this boxplot.

#### Object-level patterns
An object-level pattern for a given transition is derived by looking at the object attribute values of those objects 
passing through the selected transition. This can give you information on the distribution of the values for a given 
object attribute at a transition.


### Input Parameters

![Input Fields](res/img/inputs.png)

The following list outlines all the possible parameters that can or even need to be specified by the user:

Required parameters:

 - Transition: The user needs to select the transition that they want to find object patterns for.
 - Minimum support: Similar to fitness in process mining, minimum support is used as a threshold for deciding when a 
 given pattern in the data is strong enough to be considered a real pattern. For example, a minimum support of 1 means 
that the pattern has to occur in all events related to a transition in order to be displayed. Entering a value between 
0 and 1 is recommended and will be interpreted as a minimum support ratio threshold. 


The following parameters are optional. In case, none of them are specified, all possible object types will be considered.
For this, all attributes that can be converted to a numerical format will be considered to be numerical and binned with a
bin size of 10. 

Optional parameters:

 - Object types: This allows the user to select which object types should be considered for the discovery of patterns.
 - Attributes to consider: For each object type, the user can select which attributes they are interested in.
 - Attribute type: For each selected attribute, the user needs to label the attribute as categorical or numerical. In 
case numerical is selected, the user additionally needs to specify a bin size that will be used for the discretization.


While categorical attributes are used to find typical patterns, things aren’t as simple for numerical attributes. In 
order to discover meaningful patterns involving the numerical data as well, we preprocess these attributes by 
discretizing the data into a number of bins that can be specified by the user.
