# Installation
To make the installation as easy as possible, we rely on container technology. The application is served as a single docker image, which enables easy deployment.

## Requirements

- [docker](https://docs.docker.com/get-docker/)
- modern browser
- Optional: [docker-compose](https://docs.docker.com/compose/)

## Getting Started

### Retrieving the image

There are two ways of retrieving the image:

 - Docker Hub: `docker pull ysautter/chestnut`
 - Locally: 
      1. Download the exported image `chestnut.tar` from [here](https://drive.google.com/file/d/1E3yeGaJngVydzW27xxMbyWyavRENjpg4/view?usp=sharing) (Google Drive).
      2. Use `docker load < chestnut.tar` to load the image.
<!--The image can be retrieved either from [here]() or directly loaded via the docker hub using:  -->
<!--`docker pull ysautter/chestnut`.-->

### Starting the container

The container can be started using either of the following two methods. Both of these will start the container on port 8000.

 - `docker run -p 8000:80 ysautter/chestnut`
 - `docker-compose up` (Make sure you are in the same folder as the `docker-compose.yml` file)

!!! note
    In case of `docker-compose` make sure you downloaded the `docker-compose.yml` from [here](https://git.rwth-aachen.de/yannicksautter/chestnut/-/blob/master/docker-compose.yml) (RWTH GitLab) or [here](https://drive.google.com/file/d/1w35FXDwxWndLoerBVWVLt1S-2VahOwZf/view?usp=share_link) (Google Drive).

!!! hint
    For persistent data using the `docker run` command make sure to add a volume which points to `/data`. For example using the command: `docker volume create chestnut` and `docker run -p 8000:80 -v chestnut:/data ysautter/chestnut`.  
    Using `docker-compose`, the `docker-compose.yml` automatically takes care of creating a named volume `chestnut`. 
    


<!--Verify that the following contents are present-->
<!--```-->
<!--chestnut-->
  <!--|- docker-compose.yml-->
  <!--|- chestnut.tar-->
<!--```-->
<!--The image can either bo loaded locally using `docker load < chestnut.tar` or from the docker hub using `docker pull ysautter/chestnut`.-->

<!--Use the following command to start the container:  -->
 <!--`docker-compose up`-->

<!--The image contains a server, which is reachable by default on port 8000.-->

<!--TODO: Also include instructions on how to run the image without docker-compose-->
