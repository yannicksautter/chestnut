# Glossary
| Syntax                |                                                                                                Description                                                                                                 |
|:----------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| OCEL                  |                                                                                          Object-Centric Event Log                                                                                          |
| OCPN                  |                                                                                          Object-Centirc Petri Net                                                                                          |
| Flattening            |                                               This refers to an object-centric event log or petri net being projected to standard (case-centric) logs / nets                                               |
| Standard / Flat Log   |                                                    This refers to case-centric (i.e. non-object-centric logs) only mentioning objects of the same type                                                     |
| OF                    |                      Transitions of the OCPN are annotated with frequencies. OF refers to the object frequency which stores how many objects have passed through the given transition                      |
| EF                    |                         Transitions of the OCPN are annotated with frequencies. EF refers to the event frequency which stores the number of events related to the given transition                         |
| Object-Level Patterns |                                 Object-level patterns refer to patterns directly referring to one object (e.g. the price of one item going through a selected transition)                                  |
| Type-Level Patterns   | Type-level patterns refer to patterns about aggregated data for all objects of a specific type going through a transition (e.g. summed up weight of all items going through a transition at the same time) |


