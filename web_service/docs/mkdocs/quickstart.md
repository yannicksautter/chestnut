# Quickstart
Make sure you followed the installation process and the container is started, e.g. by using `docker ps`. On default the container should have started on port 8000. 
Use the browser to navigate to [http://localhost:8000/](http://localhost:8000/). 
On the first visit the user is greeted by the following dialog:  
<!--![Load OCEL Button](res/img/load_ocel.png)-->
<img src="../res/img/load_ocel.png" style="width: 400px"/>

First click the "Load OCEL" button, which opens following modal:

![Load OCEL Modal](res/img/load_ocel_modal_new_annotated.png)

 1. Click on "Browse", which opens the browser's file explorer, and choose a OCEL file in the `jsonocel` format
 2. By pressing "Start" the file will be uploaded to the server and processed according to the select discovery algorithm

After successfully processing the uploaded OCEL, the user will be presented with the main frame of the application.
![Main Frame](res/img/main_frame_new_annotated.png)

The first step in analyzing the data attributes of the dicovered OCPN is to select an activity by clicking on "Choose Activity". This will activate the selection mode, as indicated by the button as following:
<img src="../res/img/selection_mode.png" style="width: 150px"/>

By selecting any of the discovered activities, the user interface will display the following form:
![Enhancement Form](res/img/enhancement_form_annotated.png)
To discover patterns, a value for the minimum support for the underlying frequent item set discovery is required. By clicking on "Discover Patterns" without selecting any object type, the application will discover patterns for all object types.

!!! note
    The minimum support is a value between 0 and 1.

Afterward, the discovered patterns are displayed to the user:
![enhancements](res/img/enhancements.png)

For an indepth view on the features and use cases have a look at the respective Sections [Features](/features) and [Use Cases](/use-cases).
