# Data Aware Object Centric Petri Nets
This product is a tool for getting an in-depth view on data patterns associated with Object-Centric Petri Nets (OCPN) discovered from Object-centric Event Logs (OCELs).
Users can import an OCEL and select from a variety of discovery algorithms to discover an interactive OCPN that can be used to gain a better understanding of the process data at hand.
Users can also filter objects and events from the OCEL to discover interesting patterns, and use discovered decision points to find interesting activities to analyze.

![Index](res/img/index.png)
<!--This project enables users to get an indepth view on data attributes found-->
<!--in object-centric petri nets discovered from object-centric event logs.-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
