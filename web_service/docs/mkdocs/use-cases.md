# Use Cases
The application can be used for various purposes:

1. Discovery of OCPNs with different discovery algorithms 
2. Pattern Mining on Categorical and Numerical Attributes 
3. Object-level and Type-Level Pattern Mining
4. Pattern Mining over subprocesses
5. Pattern Mining over timespans
6. Single and side-by-side pattern mining on OCPNs
7. Pattern Mining with different fitness thresholds

## Example
In this section, we assume that you know how to import an OCEL file. Otherwise, please refer to the [Quickstart](#Quickstart).

For a first step, we want to import our OCEL "generatedDemo.jsonocel" which is a randomly generated OCEL.  
![Usecase_ocpn](res/img/usecase_ocpn.png)

Now we want to look into [Decision Points](#decision_points). The places marked with 2 and 3 are the decision points. 
![Usecase_decisionpoints](res/img/usecase_decisionpoints.png)

We can see that "failed delivery" and "package delivered" both are transitions going out of both decision points.
We can filter based on those two activities only keeping the events which go through either "failed delivery" and "package delivered". Here, we can see that in 20 events, the delivery failed; while in 103 events, the delivery was successful. 
Thus, we can conclude that in about 15% of the events, the delivery failed. 
![Usecase_filtered](res/img/usecase_filtered.png)

To investigate the two transitions further, we can use the enhancement functionality to see the differences side by side.
![Usecase_enhancement](res/img/usecase_enhancement.png)

When looking at the boxplots of products, we can see that they have high difference in the median. Thus, this suggests that more expensive products are more often delivered correctly.
![Usecase_enhancement](res/img/usecase_frequent_itemssets.png)

Additionally, we can see that 40% of the failed delivered packages are delivered to Dallas, and the USA and Japan each account for 25% of the failed deliveries. This suggests that delivering to these countries or to Dallas has major problems. 
On the other hand, we can see that in 17.4% of the successful deliveries the destination was Mexico, suggesting no issues delivering there. 
![Usecase_enhancement2](res/img/usecase_frequent_itemssets2.png)

Moreover, we should investigate how the attributes of the packages are related to the success rate of the delivery.

We can see that the upper and lower bound of the packaging costs are more close to together. This might be due to the lower frequency of failed deliveries. 

Besides that, we can see in the frequent itemsets that 40,7% of the successful deliveries are 2-day deliveries, while only 25% of the failed deliveries are 2-day deliveries.

In addition, it is visible that in 40% of the failed deliveries, the delivery service was FedEx. Furthermore, we can see that 40% of the failed deliveries are overnight deliveries. 
This suggest  that it might make sense to change the delivery service to improve the delivery success rate. 
![Usecase_enhancement3](res/img/usecase_frequent_itemssets3.png)

In this artifical example, we found out that the company has problems delivering its products. 
The reasons could be that some locations are difficult to deliver to, e.g. the USA and Japan. 
Moreover, we were able to see that successful deliveries are of products which are more expensive. 
Finally, we could see that failed deliveries are oftentimes overnight deliveries or deliveries by FedEx. 




