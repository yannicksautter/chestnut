import os

from django.views.static import serve

from web_service import settings


# Create your views here.
def serve_docs(request, path):
    docs_path = os.path.join(settings.DOCS_DIR, path)

    if os.path.isdir(docs_path):
        path = os.path.join(path, "index.html")

    path = os.path.join(settings.DOCS_STATIC_NAMESPACE, path)
    current_path = os.path.dirname(os.path.abspath(__file__))
    static_dir = os.path.join(current_path, "static")
    return serve(request, path, document_root=static_dir)
