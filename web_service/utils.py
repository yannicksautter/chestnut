import os
from pm4py.visualization.ocel.ocpn import visualizer as ocpn_visualizer

from web_service import settings


def create_img_from_ocpn(model):
    """
    @param model: pm4py ocpn model
    @return: relative in MEDIA_ROOT to image
    """
    output_file_format = "png"
    output_file_name = "test"
    gviz = ocpn_visualizer.apply(
        model,
        parameters={
            "format": output_file_format,
            "bgcolor": "white",
            "start": "initial_seed",
        },
    )

    img_dir = os.path.join(settings.MEDIA_ROOT, "ocpn_img")
    gviz.render(output_file_name, img_dir)
    return os.path.join("ocpn_img", f"{output_file_name}.{output_file_format}")
