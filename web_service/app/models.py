import copy
import json
import os

import jsonpickle
import pandas as pd
import pm4py
from django.core.files.storage import DefaultStorage

from django.db import models
from django.dispatch import receiver

from app.discovery import discover_ocpn, DiscoveryAlgorithm
from app.enhancement import Enhancement, get_decision_points, get_object_attributes, align_objects_and_events
from app.graph import pm4py_ocpn_model_to_graph
from utils import create_img_from_ocpn
from web_service import settings
from app.discovery_algorithms import check_transitions_for_duplicates


class OCPN(models.Model):
    ocel_file = models.FileField(upload_to="ocel_uploads/")
    name = models.CharField(max_length=64, null=True)
    description = models.CharField(max_length=64, null=True)
    algorithm = models.CharField(
        max_length=64,
        choices=DiscoveryAlgorithm.choices,
        default=DiscoveryAlgorithm.INDUCTIVE_MINER,
    )
    model = models.JSONField(null=True)
    graph = models.JSONField(null=True)
    img = models.ImageField(upload_to="ocpn_img/", null=True)
    duplicate_transitions = models.BooleanField(default=False)
    activity_event_frequencies = models.JSONField(null=True)
    activity_object_frequencies = models.JSONField(null=True)
    object_attributes = models.JSONField(null=True)
    decision_points = models.JSONField(null=True)

    def save(self, *args, **kwargs):
        """
        Create OCPN model and save it along the OCEL file
        """
        self.ocel_file.save(self.ocel_file.name, self.ocel_file, save=False)
        ocel = pm4py.read_ocel(self.ocel_file.path)
        ocel = align_objects_and_events(ocel)
        model = discover_ocpn(ocel, algorithm=self.algorithm)
        self.model = ocpn_serialize(model)
        self.img = create_img_from_ocpn(model)
        self.duplicate_transitions = check_transitions_for_duplicates(model)
        enhancement = Enhancement(ocel, model)
        self.activity_event_frequencies = enhancement.calc_activity_event_frequencies()
        self.activity_object_frequencies = (
            enhancement.calc_activity_object_frequencies()
        )
        self.object_attributes = get_object_attributes(enhancement.working_ocel, model)

        self.graph = pm4py_ocpn_model_to_graph(
            model, self.activity_event_frequencies, self.activity_object_frequencies
        )
        self.decision_points = get_decision_points(model)

        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)


class FilteredOCEL(models.Model):
    ocpn = models.ForeignKey(
        OCPN,
        on_delete=models.CASCADE,
    )
    file = models.FileField(upload_to="ocel_uploads/filtered/")
    activities = models.JSONField(null=True)
    fitness_threshold = models.FloatField(null=True)
    activity_event_frequencies = models.JSONField(null=True)
    activity_object_frequencies = models.JSONField(null=True)
    object_attributes = models.JSONField(null=True)
    timeFrom = models.DateTimeField(null=True)
    timeTo = models.DateTimeField(null=True)
    must_use_all_activities = models.BooleanField(default=False)
    replay_method = models.CharField(max_length=64, null=True)

    def save(self, *args, **kwargs):
        """
        Create Filtered OCEL for given filters
        """
        ocel = pm4py.read_ocel(self.ocpn.ocel_file.path)
        ocel = align_objects_and_events(ocel)
        ocpn = ocpn_deserialize(self.ocpn.model)
        enhancement = Enhancement(ocel, ocpn)
        if self.fitness_threshold:
            alignment_replay = self.replay_method and self.replay_method == "alignment"
            enhancement.apply_fitness_filter(
                fitness_threshold=self.fitness_threshold,
                alignment_replay=alignment_replay,
            )
        if self.activities and len(self.activities) != 0:
            enhancement.apply_activity_filter(
                activities=self.activities,
                must_use_all_activities=self.must_use_all_activities,
            )
        if self.timeFrom and self.timeTo:
            enhancement.apply_time_filter(
                pd.to_datetime(self.timeFrom).to_datetime64(),
                pd.to_datetime(self.timeTo).to_datetime64(),
            )
        self.object_attributes = get_object_attributes(enhancement.working_ocel, ocpn)
        self.file = write_ocel(enhancement.working_ocel)
        self.activity_event_frequencies = enhancement.calc_activity_event_frequencies()
        self.activity_object_frequencies = (
            enhancement.calc_activity_object_frequencies()
        )

        super().save(*args, **kwargs)


class EnhancementModel(models.Model):
    ocpn = models.ForeignKey(OCPN, on_delete=models.CASCADE)
    filtered_ocel = models.ForeignKey(FilteredOCEL, on_delete=models.CASCADE, null=True)
    parameters = models.JSONField()
    data = models.JSONField(null=True)
    minimum_support = models.FloatField()
    transition = models.CharField(max_length=64)

    def save(self, *args, **kwargs):
        ocpn = ocpn_deserialize(self.ocpn.model)
        if self.filtered_ocel:
            ocel_file = self.filtered_ocel.file.path
            ocel = pm4py.read_ocel(ocel_file)
        else:
            ocel_file = self.ocpn.ocel_file.path
            ocel = pm4py.read_ocel(ocel_file)
            ocel = align_objects_and_events(ocel)

        enhancement = Enhancement(ocel, ocpn)
        self.data = enhancement.discover_patterns_for_transition(
            self.transition, self.parameters, minimum_support=self.minimum_support
        )
        super().save(*args, **kwargs)


def pm4py_ocpn_model_to_json(model):
    return json.loads(jsonpickle.encode(model, make_refs=False))


def json_model_to_pm4py_model(model):
    return jsonpickle.decode(json.dumps(model))


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=OCPN)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `OCPN` object is deleted.
    """
    if instance.ocel_file:
        if os.path.isfile(instance.ocel_file.path):
            os.remove(instance.ocel_file.path)


@receiver(models.signals.pre_save, sender=OCPN)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `OCPN` object is updated
    with new file.
    """
    if not instance.pk:
        return False


def ocpn_serialize(ocpn):
    """
    Serializes the OCPN model from pm4py, such that it can be stored as JSON object.
    """
    ocpn = copy.deepcopy(ocpn)
    petri_nets = ocpn.pop("petri_nets", None)
    json_result = pm4py_ocpn_model_to_json(ocpn)
    serialized_petri_nets = {}
    for object_type, petri_net in petri_nets.items():
        # serialized_petri_nets.append(pm4py.serialize(petri_net))
        serialized_petri_nets[object_type] = pm4py.serialize(
            petri_net[0], petri_net[1], petri_net[2]
        )[1].decode()
    json_result["petri_nets"] = serialized_petri_nets
    return json_result


def ocpn_deserialize(ocpn):
    """
    Deserializes a JSON object back to the pm4py OCPN object.
    """
    ocpn = copy.deepcopy(ocpn)
    serialized_petri_nets = ocpn.pop("petri_nets", None)
    model = json_model_to_pm4py_model(ocpn)
    petri_nets = {}
    for object_type, serialized_petri_net in serialized_petri_nets.items():
        petri_nets[object_type] = pm4py.deserialize(
            ("petri_net", serialized_petri_net.encode())
        )
    model["petri_nets"] = petri_nets
    return model


def write_ocel(ocel):
    """
    Writes the OCEL to memory
    Returns: filename of the written OCEL.
    """
    storage = DefaultStorage()
    ocel_folder = "ocel"
    ocel_path = os.path.join(settings.MEDIA_ROOT, ocel_folder)
    if not os.path.exists(ocel_path):
        os.mkdir(ocel_path)

    filename = storage.get_available_name(
        os.path.join(ocel_folder, "ocel_file.jsonocel")
    )
    path = os.path.join(settings.MEDIA_ROOT, filename)
    pm4py.write_ocel_json(ocel, path)
    return filename


