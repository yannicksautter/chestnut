import json

import pm4py
import pandas as pd
import numpy as np
import math
import copy
from pm4py.algo.discovery.ocel.ocdfg.variants import classic as ocdfg_discovery
from pm4py.algo.discovery.inductive import algorithm as inductive_miner
import matplotlib.pyplot as plt
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import fpgrowth
from pm4py.algo.analysis.woflan import algorithm as woflan
from functools import reduce
from pm4py.algo.decision_mining import algorithm as decision_mining
from collections import defaultdict


def is_sound(ocpn):
    """
    Loops through petri nets of each type and checks soundness
    """
    for net, im, fm in ocpn["petri_nets"].values():

        is_sound = woflan.apply(
            net,
            im,
            fm,
            parameters={
                woflan.Parameters.RETURN_ASAP_WHEN_NOT_SOUND: True,
                woflan.Parameters.PRINT_DIAGNOSTICS: False,
                woflan.Parameters.RETURN_DIAGNOSTICS: False,
            },
        )
        if not is_sound:
            return False
    return True


def project_ocpn_on_all_object_types(ocpn):
    """
    More efficient alternative: instead of (re-)discovering flat petri nets (which only works for PM4Py's standard
    OCPN inductive miner discoveyr approach), simply modify ocpn by leaving out undesired object types. This also works
    in case of imported OCPNs
    """
    petri_nets, initial_markings, final_markings = ({}, {}, {})
    for object_type in ocpn["object_types"]:
        (
            petri_nets[object_type],
            initial_markings[object_type],
            final_markings[object_type],
        ) = ocpn["petri_nets"][object_type]
    return petri_nets, initial_markings, final_markings


def get_flat_petri_nets_for_standard_inductive_ocpn_discovery(ocel):
    """
    Creates an object-centric directly follows graph, flattens this for each object-type and uses these as input
    inductive miner to discovery flat petri nets for each object type. This ensures that the flat petri nets
    have the same structure as OCPN projected on each type.
    """
    ocdfg = ocdfg_discovery.apply(ocel)

    initial_markings = {}
    final_markings = {}
    petri_nets = {}

    for ot in ocdfg["object_types"]:
        activities_eo = ocdfg["activities_ot"]["total_objects"][ot]

        activities = {
            x: len(y) for x, y in ocdfg["activities_ot"]["events"][ot].items()
        }
        start_activities = {
            x: len(y) for x, y in ocdfg["start_activities"]["events"][ot].items()
        }
        end_activities = {
            x: len(y) for x, y in ocdfg["end_activities"]["events"][ot].items()
        }
        dfg = {x: len(y) for x, y in ocdfg["edges"]["event_couples"][ot].items()}

        (
            petri_nets[ot],
            initial_markings[ot],
            final_markings[ot],
        ) = inductive_miner.apply_dfg(dfg, start_activities, end_activities, activities)
    return petri_nets, initial_markings, final_markings


def filter_out_unfitting_events(ocel, ocpn, fitness_threshold, alignment_replay=False):
    """
    Computes traditional flat replay fitness for each trace for the flattened cases. We filter out those traces
    with a fitness below the specified threeshhold. Since in the flattened logs each trace corresponds to an object
    in the object-centric log, we filter out the objects in the OCEL.
    """
    object_types = pm4py.ocel_get_object_types(ocel)
    remaining_objects = set()
    filtered_ocels = {}

    # flatten ocel
    flattened_logs = {}
    for object_type in object_types:
        flattened_logs[object_type] = pm4py.convert_to_event_log(
            pm4py.ocel_flattening(ocel, object_type)
        )

    # flatten ocpn
    (
        flattened_petri_nets,
        initial_markings,
        final_markings,
    ) = project_ocpn_on_all_object_types(ocpn)

    # use traditional token-based replay to calculate trace fitness for each trace of each flat log/model
    # (can easily adapt to work with alignment-based fintess instead: just replace algorithm and modify "trace_fitness"
    # to "fitness" when accessing df's column)
    for object_type in object_types:
        if alignment_replay:
            replayed_traces = pm4py.conformance_diagnostics_alignments(
                flattened_logs[object_type],
                flattened_petri_nets[object_type],
                initial_markings[object_type],
                final_markings[object_type],
            )

        else:
            replayed_traces = pm4py.conformance_diagnostics_token_based_replay(
                flattened_logs[object_type],
                flattened_petri_nets[object_type],
                initial_markings[object_type],
                final_markings[object_type],
            )

        replayed_traces_df = pd.DataFrame(replayed_traces)

        # filter out traces of flattened log with fitness below threshold
        if alignment_replay:
            remaining_traces = set(
                replayed_traces_df[
                    replayed_traces_df["fitness"] >= fitness_threshold
                ].index
            )
        else:
            remaining_traces = set(
                replayed_traces_df[
                    replayed_traces_df["trace_fitness"] >= fitness_threshold
                ].index
            )
        if remaining_traces != set():
            filtered_log = [flattened_logs[object_type][i] for i in remaining_traces]
            filtered_log = pm4py.objects.log.obj.EventLog(filtered_log)

            remaining_objects = remaining_objects.union(
                set(pm4py.convert_to_dataframe(filtered_log)["case:concept:name"])
            )

    filtered_ocel = pm4py.filter_ocel_objects(ocel, remaining_objects)

    return filtered_ocel


def filter_ocel_for_transition(ocel, transition_activity):
    """
    This is the simple case where we assume no duplicate labels! This assumption allows for a simple mapping from
    transition to all events with same activity name. In case of model with no perfect fitness, a filtered log
    would be given as input
    """
    return pm4py.filter_ocel_event_attribute(
        ocel, "ocel:activity", [transition_activity]
    )


def create_object_level_df(filtered_ocel, object_type, columns=[]):
    """
    Create dataframe containing object attributes/information for each event in filtered_ocel
    """

    column_name = "ocel:type:" + str(object_type)
    # in case object type is not present in any events, ignore object type
    if column_name not in filtered_ocel.get_extended_table().columns:
        return pd.DataFrame()

    objects_df = filtered_ocel.objects
    if columns != []:
        objects_df = objects_df[list(set(columns).intersection(objects_df.columns))]
    objects_df = objects_df[objects_df["ocel:type"] == object_type].dropna(
        axis=1, how="all"
    )
    objects_df = objects_df.fillna("No Data")
    if "ocel:type" in objects_df.columns:
        objects_df.drop(columns=["ocel:type"], inplace=True)

    new_df = pd.DataFrame()

    # we iterate over all events and append objec attributes to new DataFrame
    # this can lead to repeated object columns which more realistically represents the distribution of objects
    for eid, objects in (
        filtered_ocel.get_extended_table()[column_name].dropna().items()
    ):
        new_df = pd.concat([new_df, objects_df[objects_df["ocel:oid"].isin(objects)]])

    return new_df.reset_index(drop=True)


def create_type_level_df(filtered_ocel, object_type, numerical_columns, columns=[]):
    """
    Create dataframe containing aggregated object attributes/information for each event in filtered_ocel. For each event
    in filtered_ocel, we aggregate values of mentioned objects and add them to new row in resulting table
    """
    only_object_type_ocel = pm4py.filter_ocel_object_types(filtered_ocel, [object_type])
    only_object_type_df = only_object_type_ocel.get_extended_table()

    # here we are dropping all columns containing any NaN values to ensure aggregation works without problems
    objects_df = copy.deepcopy(only_object_type_ocel.objects.dropna(axis=1, how="any"))
    if columns != []:
        objects_df = objects_df[list(set(columns).intersection(objects_df.columns))]

    if len(objects_df) == 0:
        return objects_df

    # in OCEL json/xml files, all attributes are often stored as strings.
    # Here we convert possible/desired ones to numeric values
    for column, is_numeric in objects_df.apply(
        lambda s: pd.to_numeric(s, errors="coerce").notnull().all()
    ).items():
        if is_numeric and column != "ocel:oid" and column in numerical_columns:
            objects_df.loc[:, column] = pd.to_numeric(
                objects_df[column], errors="coerce"
            )

    new_objects_dict = {}

    # aggregate attribute values
    for eid, objects in (
        only_object_type_df["ocel:type:" + str(object_type)].dropna().items()
    ):
        temp_df = objects_df[objects_df["ocel:oid"].isin(objects)]
        # only keep numeric columns since we can't easily aggregate others
        aggregated_dict = dict(temp_df.select_dtypes(include=[np.number]).sum())
        aggregated_dict["frequency"] = len(temp_df)
        new_objects_dict[eid] = aggregated_dict

    return pd.DataFrame(new_objects_dict).T.reset_index(drop=True)


def discretize_all_numerical_attributes(df, number_of_bins):
    number_of_bins = int(number_of_bins)
    # discretize numerical attributes with binning
    for column in df.select_dtypes(include=[np.number]).columns:
        if len(set(df[column])) > number_of_bins:
            df.loc[:, column] = pd.cut(
                df[column], number_of_bins
            )  # , duplicates="drop")

    return df


def discretize_numerical_attribute(df, column, number_of_bins):
    number_of_bins = int(number_of_bins)
    # discretize numerical attribute with binning
    if column in df.select_dtypes(include=[np.number]).columns:
        if len(set(df[column])) > number_of_bins:
            df.loc[:, column] = pd.cut(df[column], number_of_bins)

    return df


def find_frequent_itemsets(df, number_of_bins_per_attribute, min_support):
    """
    Find frequent itemsets above given support level in given data. For numerical columns, use binning
    number_of_bins_per_attribute = {"attr1" : n, ...}
    """

    df = copy.deepcopy(df)
    for attr in number_of_bins_per_attribute.keys():
        df = discretize_numerical_attribute(
            df, attr, number_of_bins_per_attribute[attr]
        )

    # turn all columns into strings and prefix with column name so we can distinguish equal values across attributes
    for column in df.columns:
        df[column] = df[column].apply(lambda x: column + ": " + str(x))

    te = TransactionEncoder()
    dataset = [list(i)[1:] for i in df.to_records()]

    te_ary = te.fit(dataset).transform(dataset)
    df = pd.DataFrame(te_ary, columns=te.columns_)

    return fpgrowth(df, min_support=min_support, use_colnames=True)


def get_box_plot_data(labels, bp):
    """
    Used to go from a matplotlib boxplot to the data contained in it
    """
    rows_list = []

    for i in range(len(labels)):
        dict1 = {
            "label": labels[i],
            "lower_whisker": bp["whiskers"][i * 2].get_ydata()[1],
            "lower_quartile": bp["boxes"][i].get_ydata()[1],
            "median": bp["medians"][i].get_ydata()[1],
            "upper_quartile": bp["boxes"][i].get_ydata()[2],
            "upper_whisker": bp["whiskers"][(i * 2) + 1].get_ydata()[1],
        }
        rows_list.append(dict1)

    return pd.DataFrame(rows_list)


def generate_box_plot_data(df):
    """
    Create boxplot for all numerical columns in DataFrame and convert boxplot data into DataFrame format
    """
    labels = df.select_dtypes(include=[np.number]).columns
    output_df = pd.DataFrame()
    for l in labels:
        boxplot = plt.boxplot(df[l])
        bp_df = get_box_plot_data([l], boxplot)
        output_df = pd.concat([output_df, bp_df])
    return output_df.reset_index(drop=True)


def mine_business_rule_for_transition(
    ocel, ocpn, activity, parameters, minimum_support
):
    """
    We only consider the object types and attributes that are mentioned within the attribute_parameters dictionary

    attribute_parameters = {obj_type1 : {cat_attributes: [attr1, ...], bin_sizes_for_numerical_attributes: {nattr1 : 5}} , obj_type2 : ...}
    """

    transition_ocel = filter_ocel_for_transition(ocel, activity)
    uncovertable_attributes = []

    if parameters == {}:
        # set default parameters (consider everything)
        for object_type in pm4py.ocel_get_object_types(transition_ocel):
            obj_parameters = {}

            object_type_attribute_df = ocel.objects[
                ocel.objects["ocel:type"] == object_type
            ].dropna(axis=1)
            attribute_columns = list(
                set(object_type_attribute_df.columns).difference(
                    set(["ocel:oid", "ocel:type"])
                )
            )
            object_type_attribute_df = object_type_attribute_df[attribute_columns]

            numerical_attributes = object_type_attribute_df.select_dtypes(
                include=[np.number]
            ).columns
            categorical_attributes = list(
                set(object_type_attribute_df.columns).difference(
                    set(numerical_attributes)
                )
            )

            obj_parameters["cat_attributes"] = categorical_attributes

            numerical_bin_sizes = {attr: 10 for attr in numerical_attributes}
            obj_parameters["bin_sizes_for_numerical_attributes"] = numerical_bin_sizes

            parameters[object_type] = obj_parameters

    else:
        # ensure that user only selected attributes as numerical if they can be converted
        possible_numerical_attributes = set(
            transition_ocel.objects.select_dtypes(include=[np.number]).columns
        )
        for object_type in parameters.keys():
            numerical_attributes = parameters[object_type][
                "bin_sizes_for_numerical_attributes"
            ].keys()
            if not set(numerical_attributes).issubset(possible_numerical_attributes):
                uncovertable_obj_attributes = set(numerical_attributes).difference(
                    possible_numerical_attributes
                )
                print(
                    "For "
                    + object_type
                    + ", the following attributes cannot be converted to numerical: "
                    + str(uncovertable_obj_attributes)
                )
                for attr in uncovertable_obj_attributes:
                    uncovertable_attributes.append(attr)
                    parameters[object_type]["bin_sizes_for_numerical_attributes"].pop(
                        attr
                    )

    output = {"error": ""}
    if uncovertable_attributes != []:
        output[
            "error"
        ] = "The following attributes cannot be converted to numerical: " + str(
            uncovertable_attributes
        )

    patterns = {}
    for object_type in parameters.keys():

        keep_columns = (
            list(parameters[object_type]["bin_sizes_for_numerical_attributes"].keys())
            + parameters[object_type]["cat_attributes"]
            + ["ocel:oid", "ocel:type"]
        )
        numerical_columns = parameters[object_type][
            "bin_sizes_for_numerical_attributes"
        ].keys()

        # discover object-level patterns and convert to json format
        object_level_df = create_object_level_df(
            transition_ocel, object_type, keep_columns
        )

        # no patterns to mine if there aren't any attributes or no data
        object_patterns = {}
        if len(object_level_df.columns) >= 2:
            object_patterns_df = find_frequent_itemsets(
                object_level_df,
                parameters[object_type]["bin_sizes_for_numerical_attributes"],
                minimum_support,
            )

            object_patterns = {
                "frequent item sets": json.loads(object_patterns_df.to_json()),
                "box plots": {},
            }

            for row in generate_box_plot_data(object_level_df).to_records():
                attribute = row[1]
                box_plot_data = {
                    "lower_whisker": row[2],
                    "lower_quartile": row[3],
                    "median": row[4],
                    "upper_quartile": row[5],
                    "upper_whisker": row[6],
                }
                object_patterns["box plots"][attribute] = box_plot_data

        # discover type-level patterns and convert to json format
        type_level_df = create_type_level_df(
            transition_ocel, object_type, numerical_columns, keep_columns
        )

        # no patterns to mine if ther aren't any attributes or no data
        # if len(type_level_df.columns) < 2:
        #     continue
        type_patterns_df = find_frequent_itemsets(
            type_level_df,
            parameters[object_type]["bin_sizes_for_numerical_attributes"],
            minimum_support,
        )
        type_patterns = {
            "frequent item sets": json.loads(type_patterns_df.to_json()),
            "box plots": {},
        }
        for row in generate_box_plot_data(type_level_df).to_records():
            if not any([math.isnan(x) for x in row if type(x) != str]):
                attribute = row[1]
                box_plot_data = {
                    "lower_whisker": row[2],
                    "lower_quartile": row[3],
                    "median": row[4],
                    "upper_quartile": row[5],
                    "upper_whisker": row[6],
                }
                type_patterns["box plots"][attribute] = box_plot_data

        patterns[object_type] = {
            "object patterns": object_patterns,
            "type patterns": type_patterns,
        }

    output["patterns"] = patterns
    return output


def align_objects_and_events(ocel):
    # reduce ocel to objects actually mentioned in events (in case OCEL is inconsistent)
    df = ocel.get_extended_table()

    relevant_columns = []
    for obj_type in pm4py.ocel_get_object_types(ocel):
        column_name = "ocel:type:" + obj_type
        df[column_name] = [set() if x is np.NaN else set(x) for x in df[column_name]]
        relevant_columns.append("ocel:type:" + obj_type)

    output = df[relevant_columns]
    allowed_objects = set()
    if len(output.columns) > 1:
        allowed_objects = output.aggregate(lambda x: reduce(set.union, x)).aggregate(
            lambda x: reduce(set.union, x)
        )
    else:
        for row in output.to_records():
            allowed_objects = allowed_objects.union(row[1])

    filtered_ocel = pm4py.filter_ocel_objects(ocel, list(allowed_objects))

    return filtered_ocel


class Enhancement:
    def __init__(self, ocel, ocpn):
        consistent_ocel = ocel # align_objects_and_events(ocel)
        self.original_ocel = consistent_ocel
        self.fitness_filtered_ocel = consistent_ocel
        self.working_ocel = consistent_ocel
        self.ocpn = ocpn

        self.rules = {}

    def apply_fitness_filter(self, fitness_threshold, alignment_replay=False):
        """
        default replay method: token-based
        returns output {error: ...} with message in case alignment based did not work
        """
        output = {"error": ""}
        # can only use alignment-based replay if ocpn is sound
        if alignment_replay:
            if not is_sound(self.ocpn):
                alignment_replay = False
                output["error"] = (
                    "Could not use alignment-based replay since model is not sound. Using token-based "
                    "replay instead."
                )

        self.fitness_filtered_ocel = filter_out_unfitting_events(
            self.original_ocel, self.ocpn, fitness_threshold, alignment_replay
        )
        self.working_ocel = self.fitness_filtered_ocel
        return output

    def apply_activity_filter(self, activities, must_use_all_activities=False):
        # first reset potential old filter
        # self.reset_non_fitness_filter()

        intermediate_filtered_ocel = pm4py.filter_ocel_event_attribute(
            self.working_ocel, "ocel:activity", activities, positive=True
        )
        if not must_use_all_activities:
            allowed_objects = list(set(intermediate_filtered_ocel.objects["ocel:oid"]))
            self.working_ocel = pm4py.filter_ocel_objects(
                self.working_ocel, allowed_objects
            )
        else:
            # aggregate objects by activity so that we can check which objects are present in each
            df = intermediate_filtered_ocel.get_extended_table()

            relevant_columns = ["ocel:activity"]
            for obj_type in pm4py.ocel_get_object_types(intermediate_filtered_ocel):
                column_name = "ocel:type:" + obj_type
                df[column_name] = [
                    set() if x is np.NaN else set(x) for x in df[column_name]
                ]
                relevant_columns.append("ocel:type:" + obj_type)

            df = df[relevant_columns]
            grouped = df.groupby(["ocel:activity"])
            my_lambda = lambda x: reduce(set.union, x)
            output = grouped.aggregate(
                {obj_type: my_lambda for obj_type in relevant_columns[1:]}
            )

            # intersect aggregated objects
            intersected_objects = {}
            for obj_type in relevant_columns[1:]:
                for objects in output[obj_type]:
                    if obj_type not in intersected_objects:
                        intersected_objects[obj_type] = objects
                    else:
                        intersected_objects[obj_type] = intersected_objects[obj_type].intersection(objects)

            # merge remaining objects
            allowed_objects = set()
            for objects in intersected_objects.values():
                allowed_objects = allowed_objects.union(objects)

            self.working_ocel = pm4py.filter_ocel_objects(
                self.working_ocel, allowed_objects
            )

    def apply_time_filter(self, start_time, end_time):
        # first reset potential old filter
        # self.reset_non_fitness_filter()
        self.working_ocel = pm4py.filter_ocel_events_timestamp(
            self.working_ocel, start_time, end_time
        )

    def reset_all_filters(self):
        self.working_ocel = self.original_ocel

    def reset_non_fitness_filter(self):
        self.working_ocel = self.fitness_filtered_ocel

    def calc_activity_event_frequencies(self):
        """
        Can be used to annotate transitions belonging to activity with number of events executing this activity
        """
        df = self.working_ocel.get_extended_table()
        freq = {}
        for act in set(df["ocel:activity"]):
            freq[act] = len(df[df["ocel:activity"] == act])
        return freq

    def calc_activity_object_frequencies(self):
        """
        Can be used to annotate transitions belonging to activity with number of objects executing this activity
        """
        df = self.working_ocel.get_extended_table()
        freq = {}
        for act in set(df["ocel:activity"]):
            freq[act] = len(
                pm4py.filter_ocel_event_attribute(
                    self.working_ocel, "ocel:activity", [act], positive=True
                ).objects
            )
        return freq

    def discover_patterns_for_transition(
        self, transition_name, attribute_parameters, minimum_support
    ):
        """
        We only consider the object types and attributes that are mentioned within the attribute_parameters dictionary

        attribute_parameters = {"error" : ..., "patterns" : {obj_type1 : {cat_attributes: [attr1, ...], bin_sizes_for_numerical_attributes: {nattr1 : 5}} , obj_type2 : ...}}
        """
        try:
            return mine_business_rule_for_transition(
                self.working_ocel,
                self.ocpn,
                transition_name,
                attribute_parameters,
                minimum_support,
            )
        except:
            return {"error": "Something unexpected occured"}


def get_pn_transition(pn, transition_id):
    for transition in pn.transitions:
        if transition_id == transition.name:
            return transition
    return None


def get_predecessor_places(ocpn, activity_name):
    predecessors = set()
    for object_type, pn in ocpn["petri_nets"].items():
        pn = pn[0]
        for transition in pn.transitions:
            if transition.label == activity_name:
                predecessors = predecessors.union(
                    [(object_type, arc.source) for arc in transition.in_arcs]
                )
    return predecessors


def get_decision_points(ocpn):
    decision_points_per_type = defaultdict(lambda: list())
    activities = ocpn["activities"]

    for obj_type, pn in ocpn["petri_nets"].items():
        pn = pn[0]
        dp = decision_mining.get_decision_points(pn)
        for place, transitions in dp.items():
            if (
                len(
                    [
                        trans
                        for trans in transitions
                        if get_pn_transition(pn, trans).label in activities
                    ]
                )
                > 1
            ):
                decision_points_per_type[obj_type].append(place)

    return dict(decision_points_per_type)


def get_object_attributes(ocel, ocpn):
    """
    Receives all object attributes from the ocel
    Returns: Dict, where each key is the object type and the value is a list of attributes associated
             with that object type.
    """
    object_attributes = {}
    for object_type in ocpn["object_types"]:
        object_level_df = create_object_level_df(ocel, object_type)
        attributes = __get_attributes(object_level_df)
        if len(attributes) != 0:
            object_attributes[object_type] = __get_attributes(object_level_df)
        else:
            object_attributes[object_type] = []

    return object_attributes


def __get_attributes(objects):
    """
    Helper method for retrieving
    """
    object_attributes = objects.columns.tolist()
    ocel_oid = "ocel:oid"
    ocel_type = "ocel:type"
    if ocel_oid in object_attributes:
        object_attributes.remove(ocel_oid)
    if ocel_type in object_attributes:
        object_attributes.remove(ocel_type)

    return object_attributes
