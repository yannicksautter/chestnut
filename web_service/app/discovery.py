import os.path
from enum import Enum

from django.db import models
from django.utils.translation import gettext_lazy as _
import pm4py
from pm4py.visualization.ocel.ocpn import visualizer as ocpn_visualizer

from typing import Optional, Dict, Any
from web_service.settings import STATIC_ROOT, STATIC_URL
from pm4py.algo.discovery.inductive.algorithm import Variants as i_variants
from pm4py.algo.discovery.alpha.algorithm import Variants as a_variants
from pm4py.algo.discovery.heuristics.algorithm import Variants as h_variants
from app.discovery_algorithms import apply_inductive_miner_without_DFG, apply_alpha_miner_without_DFG, apply_heuristic_miner_without_DFG


class DiscoveryAlgorithm(models.TextChoices):
    INDUCTIVE_MINER = "inductive miner classic", _("inductive miner classic")
    INDUCTIVE_MINER_INFREQUENT = "inductive miner infrequent", _("inductive miner infrequent")
    INDUCTIVE_MINER_DIRECTLY_FOLLOWS = "inductive miner directly follows", _("inductive miner directly follows")
    ALPHA_MINER_CLASSIC = "alpha miner classic", _("alpha miner classic")
    ALPHA_MINER_PLUS = "alpha miner plus", _("alpha miner plus")
    HEURISTIC_MINER_CLASSIC = "heuristic miner classic", _("heuristic miner classic")
    HEURISTIC_MINER_PLUSPLUS = "heuristic miner plus plus", _("heuristic miner plus plus")


def discover_ocpn(
        ocel, algorithm: DiscoveryAlgorithm = DiscoveryAlgorithm.INDUCTIVE_MINER,  parameters: Optional[Dict[Any, Any]] = None
):
    """
    Receives OCEL from pm4py
    returns: pm4py OCPN model
    """
    if algorithm == DiscoveryAlgorithm.INDUCTIVE_MINER:
        return apply_inductive_miner_without_DFG(ocel, variant=i_variants.IM)
    elif algorithm == DiscoveryAlgorithm.INDUCTIVE_MINER_INFREQUENT:
        return apply_inductive_miner_without_DFG(ocel, variant=i_variants.IMf)
    elif algorithm == DiscoveryAlgorithm.INDUCTIVE_MINER_DIRECTLY_FOLLOWS:
        return apply_inductive_miner_without_DFG(ocel, variant=i_variants.IMd)
    elif algorithm == DiscoveryAlgorithm.ALPHA_MINER_CLASSIC:
        return apply_alpha_miner_without_DFG(ocel, variant=a_variants.ALPHA_VERSION_CLASSIC)
    elif algorithm == DiscoveryAlgorithm.ALPHA_MINER_PLUS:
        return apply_alpha_miner_without_DFG(ocel, variant=a_variants.ALPHA_VERSION_PLUS)
    return apply_inductive_miner_without_DFG(ocel, variant=i_variants.IM)

