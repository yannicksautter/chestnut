from django.conf import settings


def product_information(request):
    return {
        "PRODUCT_NAME": settings.PRODUCT_NAME,
        "PRODUCT_NAME_SHORT": settings.PRODUCT_NAME_SHORT,
        "PRODUCT_VERSION": settings.PRODUCT_VERSION,
    }
