from django.shortcuts import render, redirect, get_object_or_404
from rest_framework import viewsets, generics, mixins, status
from rest_framework.response import Response

from app import models, serializers
from app.form import UploadOCELForm

# Create your views here.


# @login_required
def index(request):
    """
    Entry point of the Frontend. Returns the index.html with the corresponding Form for uploading the OCEL.
    """
    ocel_upload_form = UploadOCELForm()
    return render(request, "index.html", {"ocel_upload_form": ocel_upload_form})


class OCPNViewSet(viewsets.ModelViewSet):
    queryset = models.OCPN.objects.all()
    serializer_class = serializers.OCPNSerializer


class BaseViewSet(viewsets.ModelViewSet):
    """
    Basic ViewSet used for retrieving FilteredOCEL and Enhancement objects nested under the OCPN API
    """

    def get_queryset_under_ocpn(self, model):
        """
        Retrieves the queryset based on the object model under the given "ocpn_pk". The value for "ocpn_pk" is retrieved
        from the same named query parameter in the URL.
        """
        ocpn_id = self.kwargs.get("ocpn_pk")
        if not ocpn_id:
            return Response({"status": "fail"}, status=403)
        queryset_list = model.objects.filter(ocpn_id=ocpn_id)
        return queryset_list

    def create_under_ocpn(self, request, *args, **kwargs):
        """
        Sets the correct "ocpn_id" for the to be created object based on the same named query parameter in the URL.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        ocpn_id = self.kwargs.get("ocpn_pk", None)
        ocpn = models.OCPN.objects.get(pk=ocpn_id)
        serializer.validated_data["ocpn"] = ocpn
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class FilteredOCELViewSet(BaseViewSet):
    """
    A set of API Views for the FilteredOCEL Model
    """
    queryset = models.FilteredOCEL.objects.all()
    serializer_class = serializers.FilteredOCELSerializer

    def get_queryset(self):
        return self.get_queryset_under_ocpn(models.FilteredOCEL)

    def create(self, request, *args, **kwargs):
        return self.create_under_ocpn(request, *args, **kwargs)


class EnhancementViewSet(BaseViewSet):
    """
    A set of API Views for the Enhancement Model
    """
    queryset = models.EnhancementModel.objects.all()
    serializer_class = serializers.EnhancementSerializer

    def get_queryset(self):
        return self.get_queryset_under_ocpn(models.EnhancementModel)

    def create(self, request, *args, **kwargs):
        return self.create_under_ocpn(request, *args, **kwargs)


