from rest_framework import serializers

from app import models


class EnhancementSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EnhancementModel
        fields = "__all__"
        read_only_fields = ["ocpn", "data"]


class FilteredOCELSerializer(serializers.ModelSerializer):
    # enhancements = EnhancementSerializer(read_only=True, many=True)

    class Meta:
        model = models.FilteredOCEL
        fields = "__all__"
        read_only_fields = ["ocpn", "file", "activity_frequencies"]


class OCPNSerializer(serializers.ModelSerializer):
    filters = FilteredOCELSerializer(read_only=True, many=True)
    enhancements = EnhancementSerializer(read_only=True, many=True)

    class Meta:
        model = models.OCPN
        fields = "__all__"
        read_only_fields = [
            "img",
            "model",
            "graph",
            "default_filtered_ocel",
            "decision_points",
        ]
