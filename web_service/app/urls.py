from django.conf.urls.static import static
from django.urls import path, include
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularSwaggerView,
    SpectacularRedocView,
)

from rest_framework_nested import routers

from app.views import (
    index,
    OCPNViewSet,
    FilteredOCELViewSet,
    EnhancementViewSet,
)
from web_service import settings

app_name = "app"

router = routers.SimpleRouter()
router.register(r"ocpn", OCPNViewSet, basename="ocpn")


filtered_ocel_router = routers.NestedSimpleRouter(router, r"ocpn", lookup="ocpn")
filtered_ocel_router.register(
    r"filtered_ocels", FilteredOCELViewSet, basename="filtered_ocel"
)


enhancement_router = routers.NestedSimpleRouter(router, r"ocpn", lookup="ocpn")
enhancement_router.register(r"enhancements", EnhancementViewSet, basename="enhancement")

urlpatterns = [
    path("", index, name="index"),
    path("api/", include(router.urls)),
    path("api/", include(filtered_ocel_router.urls)),
    path("api/", include(enhancement_router.urls)),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="app:schema"),
        name="swagger-ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="app:schema"),
        name="redoc",
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
