from pm4py.objects.ocel.obj import OCEL
from pm4py.algo.discovery.ocel.ocdfg.variants import classic as ocdfg_discovery
from pm4py.algo.discovery.inductive import algorithm as inductive_miner
from pm4py.util import exec_utils
from collections import Counter
from typing import Optional, Dict, Any
from pm4py.objects.conversion.process_tree import converter as tree_converter
from pm4py.algo.discovery.inductive.algorithm import Variants as i_variants
from pm4py.algo.discovery.alpha.algorithm import Variants as a_variants
from pm4py.algo.discovery.ocel.ocpn.variants.wo_annotation import Parameters
from pm4py.algo.discovery.alpha import algorithm as alpha_miner
from pm4py.algo.discovery.heuristics.algorithm import Variants as h_variants
from pm4py.algo.discovery.heuristics import algorithm as heuristic_miner
import pm4py
def apply_inductive_miner_without_DFG(ocel: OCEL, parameters: Optional[Dict[Any, Any]] = None,
                                      variant: i_variants = i_variants.IMd) -> Dict[str, Any]:
    if parameters is None:
        parameters = {}

    ocdfg = ocdfg_discovery.apply(ocel, parameters=parameters)
    double_arc_threshold = exec_utils.get_param_value(Parameters.DOUBLE_ARC_THRESHOLD, parameters, 0.0)

    object_types = pm4py.ocel_get_object_types(ocel)
    flattened_logs = {}
    for object_type in object_types:
        flattened_logs[object_type] = pm4py.convert_to_event_log(
            pm4py.ocel_flattening(ocel, object_type)
        )

    petri_nets = {}
    double_arcs_on_activity = {}
    for object_type in object_types:
        process_tree = inductive_miner.apply(flattened_logs[object_type], variant=variant)
        petri_nets[object_type] = tree_converter.apply(process_tree)
        activities_eo = ocdfg["activities_ot"]["total_objects"][object_type]

        is_activity_double = {}
        for act in activities_eo:
            ev_obj_count = Counter()
            for evc in activities_eo[act]:
                ev_obj_count[evc[0]] += 1
            this_single_amount = len(list(x for x in ev_obj_count if ev_obj_count[x] == 1)) / len(ev_obj_count)
            if this_single_amount <= double_arc_threshold:
                is_activity_double[act] = True
            else:
                is_activity_double[act] = False
        double_arcs_on_activity[object_type] = is_activity_double

    ocdfg["petri_nets"] = petri_nets
    ocdfg["double_arcs_on_activity"] = double_arcs_on_activity
    return ocdfg


def apply_alpha_miner_without_DFG(ocel: OCEL, parameters: Optional[Dict[Any, Any]] = None,
                                  variant: a_variants = a_variants.ALPHA_VERSION_CLASSIC) -> Dict[str, Any]:
    if parameters is None:
        parameters = {}

    ocdfg = ocdfg_discovery.apply(ocel, parameters=parameters)
    double_arc_threshold = exec_utils.get_param_value(Parameters.DOUBLE_ARC_THRESHOLD, parameters, 0.0)

    object_types = pm4py.ocel_get_object_types(ocel)
    flattened_logs = {}
    for object_type in object_types:
        flattened_logs[object_type] = pm4py.convert_to_event_log(
            pm4py.ocel_flattening(ocel, object_type)
        )

    petri_nets = {}
    double_arcs_on_activity = {}
    for object_type in object_types:
        petri_nets[object_type] = alpha_miner.apply(flattened_logs[object_type], variant=variant)
        activities_eo = ocdfg["activities_ot"]["total_objects"][object_type]

        is_activity_double = {}
        for act in activities_eo:
            ev_obj_count = Counter()
            for evc in activities_eo[act]:
                ev_obj_count[evc[0]] += 1
            this_single_amount = len(list(x for x in ev_obj_count if ev_obj_count[x] == 1)) / len(ev_obj_count)
            if this_single_amount <= double_arc_threshold:
                is_activity_double[act] = True
            else:
                is_activity_double[act] = False
        double_arcs_on_activity[object_type] = is_activity_double

    ocdfg["petri_nets"] = petri_nets
    ocdfg["double_arcs_on_activity"] = double_arcs_on_activity

    return ocdfg


def apply_heuristic_miner_without_DFG(ocel: OCEL, parameters: Optional[Dict[Any, Any]] = None,
                                      variant: h_variants = h_variants.CLASSIC) -> Dict[str, Any]:
    if parameters is None:
        parameters = {}

    ocdfg = ocdfg_discovery.apply(ocel, parameters=parameters)
    double_arc_threshold = exec_utils.get_param_value(Parameters.DOUBLE_ARC_THRESHOLD, parameters, 0.0)

    object_types = pm4py.ocel_get_object_types(ocel)
    flattened_logs = {}
    for object_type in object_types:
        flattened_logs[object_type] = pm4py.convert_to_event_log(
            pm4py.ocel_flattening(ocel, object_type)
        )

    petri_nets = {}
    double_arcs_on_activity = {}
    for object_type in object_types:
        petri_nets[object_type] = heuristic_miner.apply(flattened_logs[object_type], variant=variant)
        activities_eo = ocdfg["activities_ot"]["total_objects"][object_type]

        is_activity_double = {}
        for act in activities_eo:
            ev_obj_count = Counter()
            for evc in activities_eo[act]:
                ev_obj_count[evc[0]] += 1
            this_single_amount = len(list(x for x in ev_obj_count if ev_obj_count[x] == 1)) / len(ev_obj_count)
            if this_single_amount <= double_arc_threshold:
                is_activity_double[act] = True
            else:
                is_activity_double[act] = False
        double_arcs_on_activity[object_type] = is_activity_double

    ocdfg["petri_nets"] = petri_nets
    ocdfg["double_arcs_on_activity"] = double_arcs_on_activity
    return ocdfg


def check_transitions_for_duplicates(ocdfg: Dict[str, Any]) -> bool:
    for object_name in ocdfg["petri_nets"]:
        petri_net = ocdfg["petri_nets"][object_name][0]
        transitions = list(petri_net.transitions)
        transition_labels = []
        for transition in transitions:
            if transition.label is not None:
                transition_labels.extend([transition.label])
        if len(transition_labels) != len(set(transition_labels)):
            return True
    return False
