import uuid

from pm4py import PetriNet
from pm4py.visualization.ocel.ocpn.variants.wo_decoration import ot_to_color


def pm4py_ocpn_model_to_graph(model, event_frequencies, object_frequencies):
    """
    Converts the pm4py discovered OCPN model into a cytoscape parsable JSON object.
    This function is highly inspired by the pm4py OCPN model to graphviz function.
    """
    graph_data = []
    activities_map = {}
    source_places = {}
    target_places = {}
    transition_map = {}
    places = {}

    for act in model["activities"]:
        activity_uuid = str(uuid.uuid4())
        activities_map[act] = activity_uuid
        graph_data.append(
            {
                "data": {
                    "id": activity_uuid,
                    "label": f"{act} (EF: {event_frequencies[act]} | OF: {object_frequencies[act]})",
                },
                "classes": ["center-center", "activity"],
            }
        )

    for ot in model["object_types"]:
        otc = ot_to_color(ot)
        source_uuid = str(uuid.uuid4())
        target_uuid = str(uuid.uuid4())
        source_places[ot] = source_uuid
        target_places[ot] = target_uuid
        graph_data.append(
            {
                "data": {
                    "id": source_uuid,
                    "label": ot,
                },
                "classes": ["center-center", "source"],
                "style": {
                    "background-color": otc,
                },
            }
        )
        graph_data.append(
            {
                "data": {
                    "id": target_uuid,
                    "label": ot,
                },
                "classes": ["center-center", "target"],
                "style": {
                    "background-color": otc,
                },
            }
        )

    for ot in model["petri_nets"]:
        otc = ot_to_color(ot)
        net, im, fm = model["petri_nets"][ot]
        for place in net.places:
            if place in im:
                places[place] = source_places[ot]
            elif place in fm:
                places[place] = target_places[ot]
            else:
                place_uuid = str(uuid.uuid4())
                places[place] = place_uuid
                graph_data.append(
                    {
                        "data": {
                            "id": place_uuid,
                            "label": " ",
                            "place": place.name,
                            "ot": ot,
                        },
                        "classes": ["center-center", "place"],
                        "style": {
                            "background-color": otc,
                        },
                    }
                )
        for trans in net.transitions:
            if trans.label is not None:
                transition_map[trans] = activities_map[trans.label]
            else:
                transition_uuid = str(uuid.uuid4())
                transition_map[trans] = transition_uuid
                graph_data.append(
                    {
                        "data": {
                            "id": transition_uuid,
                            "label": " ",
                        },
                        "classes": ["center-center", "transition"],
                        "style": {
                            "background-color": otc,
                        },
                    }
                )

        for arc in net.arcs:
            if type(arc.source) is PetriNet.Place:
                is_double = (
                    arc.target.label in model["double_arcs_on_activity"][ot]
                    and model["double_arcs_on_activity"][ot][arc.target.label]
                )
                penwidth = "4.0" if is_double else "1.0"
                graph_data.append(
                    {
                        "data": {
                            "source": places[arc.source],
                            "target": transition_map[arc.target],
                        },
                        "style": {
                            "line-color": otc,
                            "target-arrow-color": otc,
                            "width": penwidth,
                        },
                    }
                )
            elif type(arc.source) is PetriNet.Transition:
                is_double = (
                    arc.source.label in model["double_arcs_on_activity"][ot]
                    and model["double_arcs_on_activity"][ot][arc.source.label]
                )
                penwidth = "4.0" if is_double else "1.0"

                graph_data.append(
                    {
                        "data": {
                            "source": transition_map[arc.source],
                            "target": places[arc.target],
                        },
                        "style": {
                            "line-color": otc,
                            "target-arrow-color": otc,
                            "width": penwidth,
                        },
                    }
                )
    return graph_data
