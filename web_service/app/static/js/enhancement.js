const resetEnhancementForm = function (suffix, selection_mode) {
    // This function resets the enhancement form for a given suffix and selection mode
    const enhancementForm = document.getElementById(`enhancementForm-${suffix}`);
    enhancementForm.innerHTML = `
        <div class="row justify-content-center">
            <div class="col-auto">
                <button type="button" class="btn btn-outline-primary" id="enhancementSelectionModeButton-${suffix}">
                    <i class="fas fa-plus"></i> Choose Activity
                </button>
            </div>
        </div>
    `
    //Reset the container content
    const enhancementContainer = document.getElementById(`enhancementContainer-${suffix}`);
    enhancementContainer.innerText = '';

    //Reinitialize the corresponding selection buttons
    initEnhancementSelectionModeButton(`enhancementSelectionModeButton-${suffix}`, selection_mode);
}
const showEnhancementContainer = function () {
    const enhancementContainer = document.getElementById("enhancementContainer");
    show(enhancementContainer);
    resetEnhancementForm("left", SELECTION_MODE_ENHANCEMENT_LEFT);
    resetEnhancementForm("right", SELECTION_MODE_ENHANCEMENT_RIGHT);
}

const createBoxPlot = function (tp_boxplot_data, boxplot_attr, typePatternsDiv, displayBoxplotFunctions) {
    //Helper function to create boxplots
    const boxplot_data = tp_boxplot_data[boxplot_attr]
    if (!boxplot_data) {
        return;
    }
    const col = document.createElement('div');
    col.setAttribute('class', 'col-auto col-boxplot me-5')

    const tp_boxplot = document.createElement('div');
    const tp_boxplot_id = `tp-boxplot-${guidGenerator()}`;
    tp_boxplot.setAttribute('id', tp_boxplot_id);

    col.appendChild(tp_boxplot);
    typePatternsDiv.appendChild(col);
    displayBoxplotFunctions.push(() => {
        displayBoxplot(tp_boxplot_id, boxplot_attr, boxplot_data);
    })
}

const createFrequentItemSetsTable = (pattern_data, rowFrequentItemSets, object_type, pt, suffix, displayFrequentItemSetsFunctions) => {
    const support_data = pattern_data['support']
    const itemsets_data = pattern_data['itemsets']
    const frequent_item_sets = []
    for (const index in support_data) {
        frequent_item_sets.push({
            support: support_data[index],
            itemsets:  itemsets_data[index]
        })
    }

    rowFrequentItemSets.innerHTML = `
            <div class="persist d-flex justify-content-center">
                <hr class="flex-grow-1 me-1">
                <button class="persist btn btn-outline-info border-0" data-bs-target="#frequent_item_sets-${object_type}-${pt}-${suffix}" data-bs-toggle="collapse">Frequent Item Sets ▼ <i type="button" data-bs-toggle="tooltip" title="Frequent itemsets for a given transition describe common patterns that can be found in the attribute values of the objects related to the selected transition" class="fa fa-info-circle"></i></button>
                <hr class="flex-grow-1 ms-1">
            </div>
            <div id="frequent_item_sets-${object_type}-${pt}-${suffix}" class="persist collapse mb-3">
                <table>
                <table class="table w-75" id="frequent_item_sets-${object_type}-${pt}-${suffix}-table">
                  <thead>
                    <tr>
                      <th scope="col" class="support text-center">Support</th>
                      <th scope="col" class="itemsets">Item Sets</th>
                    </tr>
                  </thead>
                </table>
            </div>
    `
    displayFrequentItemSetsFunctions.push(() => {
        new DataTable(`#frequent_item_sets-${object_type}-${pt}-${suffix}-table`, {
            searching: false,
            paging: false,
            bInfo: false,
            data: frequent_item_sets,
            order: [[0, 'desc']],
            columnDefs: [
                {
                    targets: "support",
                    data: "support",
                    className: 'text-center'
                },
                {
                    targets: "itemsets",
                    data: "itemsets",
                },
            ],
        });
    })
}

function displayPatterns(object_type, data, pattern_type, displayBoxplotFunctions, displayFrequentItemSetsFunctions, suffix) {
    const patternsDiv = document.createElement('div');
    patternsDiv.setAttribute('class', 'row row-patterns')

    const op_title = document.createElement('p');
    if (pattern_type=="object patterns") {
        op_title.innerHTML = `
        <label> Object-Level Patterns
        <i type="button" data-bs-toggle="tooltip" title="An object-level pattern for a given transition is derived by looking at the object attribute values of those objects passing through the selected transition. This can give you information on the distribution of the values for a given object attribute at a transition" class="fa fa-info-circle"></i>
        </label>`
    }
    else {
        op_title.innerHTML = `
        <label> Type-Level Patterns
        <i type="button" data-bs-toggle="tooltip" title="A type-level pattern is derived by aggregating object attribute values of all objects passing through a transition at the same time. For example, consider 4 items related to an object-centric event with the “create package” activity name. In this case, a type-level pattern could, for instance, give you information on the sum of the weight of all items related to one execution of the transition labeled with “create package”. All attribute values of each object attribute related to an event are summed up for the aggregation. Due to this, only numerical attributes are considered here. \n \nThe frequency boxplot depicts the distribution of the number of objects related to an execution of the chosen transition. For example, in case there always are exactly 4 items related to a 'create package' event, this will be visible in this boxplot" class="fa fa-info-circle"></i>
        </label>`
    }
    patternsDiv.appendChild(op_title);

    const rowBoxplots = document.createElement('div')
    rowBoxplots.setAttribute('class', 'row row-boxplots justify-content-start')

    const boxplot_data = data['patterns'][object_type][pattern_type]['box plots']

    if (boxplot_data != null) {
        const { frequency, ...boxplot_data_wo_frequency } = boxplot_data
        createBoxPlot(boxplot_data, 'frequency', rowBoxplots, displayBoxplotFunctions);

        for (const boxplot_attr in boxplot_data_wo_frequency) {
            createBoxPlot(boxplot_data, boxplot_attr, rowBoxplots, displayBoxplotFunctions);
        }
    }

    const rowFrequentItemSets = document.createElement('div')
    rowFrequentItemSets.setAttribute('class', 'row row-frequentItemSets')

    const pt = pattern_type.replace(" ", "_")

    //-------------------
    //createFrequentItemsTable();
    const pattern_data = data['patterns'][object_type][pattern_type]['frequent item sets']

    if (pattern_data != null) {
        createFrequentItemSetsTable(pattern_data,rowFrequentItemSets, object_type, pt, suffix, displayFrequentItemSetsFunctions);
    }

    //-------------------

    const colContainer = document.createElement('div')
    colContainer.setAttribute('class', 'col col-container')

    colContainer.appendChild(rowBoxplots);
    colContainer.appendChild(rowFrequentItemSets);

    patternsDiv.appendChild(colContainer)


    return patternsDiv
}


const showEnhancementData = function(enhancement, label, suffix){
    if (!enhancement) {
        return
    }


    const data = enhancement.data;
    const enhancementContainer = document.getElementById(`enhancementContainer-${suffix}`);
    enhancementContainer.innerText = ''

    if (data['error'] !== '') {
        showToastMessage("Error", "", data['error'])
        return;
    }

    let displayBoxplotFunctions = [];
    let displayFrequentItemSetsFunctions = [];

    for (const object_type in data['patterns']) {
        if (object_type === 'error') {
            continue;
        }
        const rowObjectTypes = document.createElement('div');
        rowObjectTypes.setAttribute('class', 'row mt-3');

        const title = document.createElement('h5');
        title.setAttribute('class', 'card-title');
        title.innerHTML = `
        <h5> Object Type ${object_type}
        <i type="button" data-bs-toggle="tooltip" title="Boxplots show the distribution of the numerical attribute values. They display extreme values, the median, as well as the 1st and 3rd quartile of the distribution." class="fa fa-info-circle"></i>
        </h5>`

        const objectPatternsDiv = displayPatterns(object_type, data, 'object patterns', displayBoxplotFunctions, displayFrequentItemSetsFunctions, suffix);
        const typePatternsDiv = displayPatterns(object_type, data, 'type patterns', displayBoxplotFunctions, displayFrequentItemSetsFunctions, suffix);

        const body = document.createElement('div');
        body.setAttribute('class', 'card-body');

        const card = document.createElement('card');
        card.setAttribute('class', 'card');
        // card.style.width = '25rem';

        const col = document.createElement('div');
        col.setAttribute('class', 'col');

        body.appendChild(title);

        body.appendChild(objectPatternsDiv);
        body.appendChild(document.createElement('hr'));
        body.appendChild(typePatternsDiv);
        // body.appendChild(row);

        card.appendChild(body);

        col.appendChild(card);

        rowObjectTypes.appendChild(col);

        enhancementContainer.appendChild(rowObjectTypes);
    }

    for (const displayFunction of displayBoxplotFunctions) {
        displayFunction();
    }
    for (const displayFunction of displayFrequentItemSetsFunctions) {
        displayFunction();
    }

    initTooltips()
}


const displayBoxplot = function (boxplot_id, label, data) {

    if (!data) {
        return;
    }

    // set the dimensions and margins of the graph
    let margin = {top: 10, right: 30, bottom: 30, left: 40},
      width = 200 - margin.left - margin.right,
      height = 200 - margin.top - margin.bottom;

    let q1 = data['lower_quartile'];
    let median = data['median'];
    let q3 = data['upper_quartile'];
    let min = data['lower_whisker'];
    let max = data['upper_whisker'];

    // append the svg object to the body of the page
    let svg = d3.select(`#${boxplot_id}`)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // Show the Y scale
    let y = d3.scaleLinear()
        .domain([min*0.8,max*1.2])
        .range([height, 0]);
    svg.append('g').call(d3.axisLeft(y))

    // add a title
    svg.append("text")
        .attr("x", (width / 2) + 10)
        .attr("y", (margin.top / 2))
        .attr("text-anchor", "middle")
        .style("font-size", "18px")
        //.style("text-decoration", "underline")
        .text(label);

    // // Y axis label:
    // svg.append("text")
    //     .attr("text-anchor", "end")
    //     .attr("transform", "rotate(-90)")
    //     .attr("y", -margin.left+20)
    //     .attr("x", -margin.top)
    //     .text(label)

    // a few features for the box
    let center = 100
    let box_width = 50

    // Show the main vertical line
    svg
    .append("line")
      .attr("x1", center)
      .attr("x2", center)
      .attr("y1", y(min) )
      .attr("y2", y(max) )
      .attr("stroke", "black")

    // Show the box
    svg
    .append("rect")
      .attr("x", center - box_width/2)
      .attr("y", y(q3) )
      .attr("height", (y(q1)-y(q3)) )
      .attr("width", box_width )
      .attr("stroke", "black")
      .style("fill", stringToColour(label))

    // show median, min and max horizontal lines
    svg
    .selectAll("toto")
    .data([min, median, max])
    .enter()
    .append("line")
      .attr("x1", center-box_width/2)
      .attr("x2", center+box_width/2)
      .attr("y1", function(d){ return(y(d))} )
      .attr("y2", function(d){ return(y(d))} )
      .attr("stroke", "black")

}

// const handleEnhancementResponse = function(result) {
//     // data = result["data"];
//     enhancement = result;
//     // getAllTransitions(result);
//     // handleDropdown();
// }
const showEnhancementForm = function (transition, suffix) {
    const enhancementWrapper = document.getElementById(`enhancementWrapper-${suffix}`);
    constructEnhancementForm(object_attributes, transition, suffix);
    show(enhancementWrapper);
    const enhancementContainer = document.getElementById(`enhancementContainer-${suffix}`)
    enhancementContainer.innerText = '';
}

const constructEnhancementForm = function (object_attributes, transition, suffix) {
    const enhancementForm = document.getElementById(`enhancementForm-${suffix}`);

    enhancementForm.innerText = '';
    enhancementForm.innerHTML = `

                <div class="row">
                    <div class="col-auto me-auto">
                        <h3 class="mb-3 mt-3">Enhancements for transition: ${transition}</h3>
                    </div>
                    <div class="col-auto ms-auto align-self-center">
                        <button type="button" id="enhancementSelectionModeButton-${suffix}" class="btn btn-outline-primary ms-auto">
                            Change Activity
                        </button>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="minimumSupportInput-${suffix}" class="col-auto col-form-label">Minimum Support: <i type="button" data-bs-toggle="tooltip" title="Similar to fitness in process mining, minimum support is used as a threshold for deciding when a given pattern in the data is strong enough to be considered a real pattern. For example, a minimum support of 1 means that the pattern has to occur in all events related to a transition in order to be displayed" class="fa fa-info-circle"></i></label>
                    <div class="col-2">
                      <input type="text" class="form-control" id="minimumSupportInput-${suffix}" value="0.25">
                    </div>
                </div>
                
                <div class="row mb-1">
                    <label class="col-auto">Select object types:</label>
                </div>
                
                <div class="row mb-3">
                    <div class="col ms-3">
                        <div id="enhancementFormContainer-${suffix}">
                        </div>
                    </div>
                </div>
    `
    const selection_mode = suffix === 'left' ? SELECTION_MODE_ENHANCEMENT_LEFT : SELECTION_MODE_ENHANCEMENT_RIGHT
    initEnhancementSelectionModeButton(`enhancementSelectionModeButton-${suffix}`, selection_mode)

    const enhancementFormContainer = document.getElementById(`enhancementFormContainer-${suffix}`);
    Object.keys(object_attributes).forEach(function(object_type, index) {
        const attributes = object_attributes[object_type];
        const objectContainer = document.createElement('div');
        const objIdPrefix = `enh_ot_${object_type}-${suffix}`
        objectContainer.innerHTML = `
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="${objIdPrefix}_checkbox" data-bs-target="#${objIdPrefix}_collapse" data-bs-toggle="collapse">
                <label class="form-check-label" for="${objIdPrefix}_checkbox">
                    ${object_type}
                </label>
            </div>
            <div class="collapse" id="${objIdPrefix}_collapse">
             </div>
        `
        enhancementFormContainer.appendChild(objectContainer);
        if (attributes.length === 0) {
            return;
        }
        document.getElementById(`${objIdPrefix}_collapse`).innerHTML = `
                 <div class="card card-body mt-3 mb-3 ms-3">
                     <div class="row">
                         <label class="col-auto">Define object attributes: <i type="button" data-bs-toggle="tooltip" title="While categorical attributes are used to find typical patterns, things aren’t as simple for numerical attributes. In order to discover meaningful patterns involving the numerical data as well, we preprocess these attributes by discretizing the data into a number of bins specified by the user" class="fa fa-info-circle"></i></label>

                         <div class="row mb-3">
                             <div class="col ms-3">
                                 <div id="${objIdPrefix}_attrs">
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
        `
        const attributesElement = document.getElementById(`enh_ot_${object_type}-${suffix}_attrs`)
        for (const attr of attributes) {
            const attributeContainer = document.createElement('div');
            const attrIdPrefix = `${objIdPrefix}_attr_${attr}`
            attributeContainer.innerHTML = `
                 <div class="row align-items-center mt-3">
                     <div class="col-auto form-check">
                         <input class="form-check-input" type="checkbox" value="" id="${attrIdPrefix}_checkbox">
                         <label class="form-check-label" for="${attrIdPrefix}_checkbox">
                             ${attr}
                         </label>
                     </div>
                     <div class="col-auto">
                         <select class="form-select" aria-label="Default select example" id="${attrIdPrefix}_select">
                             <option selected value="numerical">Numerical</option>
                             <option value="categorical">Categorical</option>
                         </select>
                     </div>
                     <div class="col-auto" id="${attrIdPrefix}_binSize_container">
                         <label class="visually-hidden" for="binSize">Bin Size</label>
                         <input type="text" class="form-control"  id="${attrIdPrefix}_binSize" placeholder="Bin Size">
                     </div>
                 </div>
            `
            attributesElement.appendChild(attributeContainer);
            document.getElementById(`${attrIdPrefix}_select`).onchange = (ev) => {
                const value = ev.target.value;
                const binSizeContainer = document.getElementById(`${attrIdPrefix}_binSize_container`);
                if (value === 'numerical') {
                    show(binSizeContainer);
                } else {
                    hide(binSizeContainer);
                }
            }

        }


    })
    const discoverPatternsButton = document.createElement('button');
    discoverPatternsButton.setAttribute('type', 'button');
    discoverPatternsButton.setAttribute('class', 'btn btn-primary');
    discoverPatternsButton.setAttribute('class', 'btn btn-primary');

    discoverPatternsButton.innerHTML = `
    <label> Discover Patterns
    <i type="button" data-bs-toggle="tooltip" title="This enhancement deals with discovering what the object-related data looks like at a given transition in the object-centric Petri net" class="fa fa-info-circle"></i>
    </label>`

    const loadIcon = getLoadIcon();
    discoverPatternsButton.onclick = (ev) => {
        ev.preventDefault();
        discoverPatternsButton.after(loadIcon);


        let parameters = {};
        Object.keys(object_attributes).forEach(function(object_type, index) {
            const objIdPrefix = `enh_ot_${object_type}-${suffix}`
            const object_type_checked = document.getElementById(`${objIdPrefix}_checkbox`).checked
            if (!object_type_checked) {
                return
            }
            parameters[object_type] = {
                "cat_attributes": [],
                "bin_sizes_for_numerical_attributes": {}
            }
            const attributes = object_attributes[object_type];
            for (const attr of attributes) {
                const attrIdPrefix = `${objIdPrefix}_attr_${attr}`
                const attr_checked = document.getElementById(`${attrIdPrefix}_checkbox`).checked
                if (!attr_checked) {
                    continue;
                }
                const attr_type = document.getElementById(`${attrIdPrefix}_select`).value
                if (attr_type === 'categorical') {
                    parameters[object_type]["cat_attributes"].push(attr)
                } else {
                    const binSize = document.getElementById(`${attrIdPrefix}_binSize`).value
                    parameters[object_type]["bin_sizes_for_numerical_attributes"][attr] = binSize
                }
            }
        })
        const minimum_support = document.getElementById(`minimumSupportInput-${suffix}`).value
        if (minimum_support === "") {
            showToastMessage("Form Error", "", "Value for minimum support is required")
            return
        }
        let data = {
            transition: transition,
            minimum_support: minimum_support,
            parameters: parameters,
            filtered_ocel: current_filtered_ocel
        }
        fetch(getAPI({
            path: `ocpn/${current_ocpn_id}/enhancements/`
        }), {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((result) => {
                console.log('Success:', result);
                showEnhancementData(result, transition, suffix)
                removeLoadIcon();
            })
            .catch((error) => {
                console.error('Error:', error);
                showToastMessage("API Error", "", error)
                removeLoadIcon();
            });
    }

    enhancementForm.append(discoverPatternsButton);

    initTooltips()
}

const initEnhancementSelectionModeButton = (selectionModeButtonId, selectionMode) => {
    const selectionModeButton = document.getElementById(selectionModeButtonId);
    selectionModeButton.onclick = (ev) => {
        ev.preventDefault();
        toggleSelectionMode(selectionMode);
    }
}

initEnhancementSelectionModeButton('enhancementSelectionModeButton-left', SELECTION_MODE_ENHANCEMENT_LEFT);
initEnhancementSelectionModeButton('enhancementSelectionModeButton-right', SELECTION_MODE_ENHANCEMENT_RIGHT);


