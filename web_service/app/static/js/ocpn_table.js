let table = new DataTable('#OCPNTable', {
     searching: false,
     paging: false,
     bInfo: false,
     select: {
          style: "single"
     },
     ajax: {
          url: '/api/ocpn/',
          dataSrc: ''
     },
     columnDefs: [
          {
               targets: "id",
               data: "id",
          },
          {
               targets: "name",
               data: "name",
          },
          {
               targets: "description",
               data: "description"
          },
          {
               targets: "filename",
               data: "ocel_file",
               render: function (data, type, row) {
                    return fileFromPath(data);
               },
          },
          {     targets: "discovery_algorithm",
                data: "algorithm",
          },
          {
               targets: "file",
               data: "ocel_file",
               render: function (data, type, row) {
                    return `<a href="${data}"><i class="fas fa-file-download" data-bs-toggle="tooltip" title="Download ${data}"></i>`;
               },
               className: 'text-center'
          },
          {
               target: "action",
               render: function (data, type, row) {
                    return '<i class="fas fa-trash"></i>';
               }
          }
    ],
     createdRow: function (row, data, dataIndex) {
          let trash_icon = row.getElementsByClassName('fa-trash')[0];
          trash_icon.style.cursor = 'pointer';
          trash_icon.onclick = () => {
               fetch(getAPI({
                    path: `ocpn/${data.id}/`,
               }), {
                    method: 'DELETE',
               })
                   .then((response) => {
                        console.log('Success:', response);
                        table.ajax.reload();
                   })
                   .then((result) => {
                   })
                   .catch((error) => {
                        console.error('Error:', error);
                   });
          }

     }
});
// $('#OCPNTable tbody').on('click', 'tr', function () {
//      if ($(this).hasClass('selected')) {
//           $(this).removeClass('selected')'Cli'Click'ck';
//      } else {
//           table.$('tr.selected').removeClass('selected');
//           $(this).addClass('selected');
//      }
// });
