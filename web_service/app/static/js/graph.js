//https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
String.prototype.hashCode = function() {
  let hash = 0,
    i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr = this.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
}

let graph = null;

const displayGraph = function (ocpn_graph) {

    console.log("Display Graph")
    console.log(ocpn_graph)

    const OCPNGraph = document.createElement('div');
    OCPNGraph.setAttribute('id', 'OCPNInteractiveGraph');
    OCPNGraph.setAttribute('class', 'w-100')
    OCPNGraph.style.height = "700px";
    //OCPNGraph.style.height = "43rem";
    OCPNGraph.style.backgroundColor = "#ececec"


    graph = cytoscape({
        container: OCPNGraph,
        elements: ocpn_graph,
        wheelSensitivity: 0.04,
        style: [ // the stylesheet for the graph
            {
                selector: ".center-center",
                style: {
                    "text-valign": "center",
                    "text-halign": "center"
                }
            },
            {
                selector: 'node',
                style: {
                    'background-color': '#666',
                    'label': 'data(label)'
                }
            },

            {
                selector: 'edge',
                style: {
                    'width': 3,
                    'line-color': '#ccc',
                    'target-arrow-color': '#ccc',
                    'target-arrow-shape': 'triangle',
                    'curve-style': 'bezier'
                }
            },
            {
                selector: '.activity',
                style: {
                    'label': 'data(label)',
                    'shape': 'rectangle',
                    'width': (node) => { return node.data('label').length * 9 },
                    'height': '50',
                    'background-color': 'white',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-color': 'black'
                }
            },
            {
                selector: '.source',
                style: {
                    'label': 'data(label)',
                    'shape': 'ellipse',
                    // 'width': (node) => { return node.data('label').length * 9 },
                    'width': '100',
                    'height': '50',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-color': 'black'
                }
            },
            {
                selector: '.target',
                style: {
                    'label': 'data(label)',
                    'shape': 'diamond',
                    'width': '100',
                    'height': '50',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-color': 'black'
                }
            },
            {
                selector: '.place',
                style: {
                    'label': 'data(label)',
                    'shape': 'ellipse',
                    'width': '50',
                    'height': '50',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-color': 'black'
                }
            },
            {
                selector: '.transition',
                style: {
                    'label': 'data(label)',
                    'shape': 'rectangle',
                    'width': '100',
                    'height': '50',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-color': 'black'
                }
            },
            {
                selector: '.selected',
                style: {
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-color': '#FFA07A'
                }
            },
            {
                selector: '.decision_point',
                style: {
                    'border-width': '3px',
                    'border-style': 'solid',
                    'border-color': '#ff4242'
                }
            },
            {
                selector: '.fade',
                style: {
                    'opacity': 0.3,
                }
            },
        ],

        layout: {
            name: 'elk',
            elk: {
                 algorithm: 'layered',
                'elk.direction': 'RIGHT',
            },
        },
    })
    document.addEventListener('scroll', (e) => {
      graph.resize();
    });
    graph.on('tap', '.activity', function(evt){
      let node = evt.target;
      switch (current_selection_mode) {
          case SELECTION_MODE_DISABLED:
              return;
          case SELECTION_MODE_FILTER:
              toggleSelected(node);
              break;
          case SELECTION_MODE_ENHANCEMENT_LEFT:
              current_selection_mode = SELECTION_MODE_DISABLED;
              showEnhancementForm(getTransitionFromLabel(node.data().label), 'left');
              break;
          case SELECTION_MODE_ENHANCEMENT_RIGHT:
              current_selection_mode = SELECTION_MODE_DISABLED;
              showEnhancementForm(getTransitionFromLabel(node.data().label), 'right');
              break;
      }
    });
    return OCPNGraph;
}

// const ot_to_color = function (ot) {
//     let ot_hash = ot.hashCode();
//     let num = [];
//     while (num.length < 6) {
//         num.push(ot_hash % 16);
//         ot_hash = Math.floor(ot_hash / 16);
//     }
//     let ret = "#";
//     for (const n of num) {
//         let hex_represenation = n.toString(16);
//         ret = `${ret}${hex_represenation}`
//     }
//     return ret
// }

// graph.style().selector('edge').style({
//     "curve-style": "unbundled-bezier",
//     "control-point-distances": 120,
//     "control-point-weights": 0.1,
// }).update()
// let activities_map = {}
// let source_places = {}
// let target_places = {}
// let transition_map = {}
// let places = {}
// for (const act of ocpn_model['activities']['py/set']) {
//     const uuid = self.crypto.randomUUID();
//     activities_map[act] = uuid;
//     graph.add({
//             data: {
//                 id: uuid,
//                 label: act,
//             },
//             classes: [
//                 'center-center',
//                 'activity'
//             ]
//         }
//     )}
// for (const ot of ocpn_model['object_types']['py/set']) {
//     const otc = ot_to_color(ot);
//     const source_uuid = self.crypto.randomUUID();
//     const target_uuid = self.crypto.randomUUID();
//     source_places[ot] = source_uuid;
//     target_places[ot] = target_uuid;
//     graph.add({
//         data: {
//             id: source_uuid,
//             label: ot,
//         },
//         classes: [
//             'center-center',
//             'source'
//         ],
//         style: {
//             'background-color': otc,
//         }
//     })
//     graph.add({
//         data: {
//             id: target_uuid,
//             label: ot,
//         },
//         classes: [
//             'center-center',
//             'target'
//         ],
//         style: {
//             'background-color': otc,
//         }
//     })
//
// }
// for (const ot in ocpn_model['petri_nets']) {
//     const otc = ot_to_color(ot);
//     const [net, im, fm] = ocpn_model['petri_nets'][ot]['py/tuple'];
//     const petriNet_places = net['_PetriNet__places']['py/set'];
//     const petriNet_transitions = net['_PetriNet__transitions']['py/set'];
//     const petriNet_arcs = net['_PetriNet__arcs']['py/set'];
//     for (const place of petriNet_places) {
//         const place_name = place['_Place__name']
//         if (place_name in im) {
//             places[place_name] = source_places[ot]
//         } else if (place_name in fm) {
//             places[place_name] = target_places[ot]
//         } else {
//             const place_uuid = self.crypto.randomUUID();
//             places[place_name] = place_uuid;
//             graph.add({
//                 data: {
//                     id: place_uuid,
//                     label: place_name,
//                 },
//                 classes: [
//                     'center-center',
//                     'place'
//                 ],
//                 style: {
//                     'background-color': otc,
//                 }
//             })
//         }
//     }
//     for (const trans of petriNet_transitions) {
//         const trans_label = trans['_Transition__label']
//         const trans_name = trans['_Transition__name']
//         if (trans_label != null) {
//             transition_map[trans_name] = activities_map[trans_label]
//         } else {
//             let transition_uuid = self.crypto.randomUUID();
//             transition_map[trans_name] = transition_uuid;
//             graph.add({
//                 data: {
//                     id: transition_uuid,
//                     label: ' ',
//                 },
//                 classes: [
//                     'center-center',
//                     'transition'
//                 ],
//                 style: {
//                     'background-color': otc,
//                 }
//             })
//         }
//     }
//     console.log(transition_map);
//     console.log(places);
//     for (const arc of petriNet_arcs) {
//         const arc_source = arc['_Arc__source']
//         const arc_target = arc['_Arc__target']
//         if (arc_source['py/object'] === 'pm4py.objects.petri_net.obj.PetriNet.Place') {
//             // const is_double = arc['_Place__2']
//             console.log(arc)
//             const transition_uuid = self.crypto.randomUUID();
//             const place_name = arc_source['_Place__name'];
//             console.log(`Create edge from ${place_name} to ${arc_target}`)
//             console.log(`Create edge from ${places[place_name]} to ${transition_map[arc_target]}`)
//             graph.add({
//                 data: {
//                     id: transition_uuid,
//                     source: places[place_name],
//                     target: transition_map[arc_target],
//                     label: ' '
//                 },
//                 classes: [
//                     'center-center',
//                     'edge'
//                 ],
//                 style: {
//                     'background-color': otc,
//                 }
//             })
//         }
//         if (arc_source['py/object'] === 'pm4py.objects.petri_net.obj.PetriNet.Transition') {
//             const transition_uuid = self.crypto.randomUUID();
//             graph.add({
//                 data: {
//                     id: transition_uuid,
//                     source: transition_map[arc_source],
//                     target: places[arc_target]
//                 },
//                 classes: [
//                     'edge'
//                 ],
//                 style: {
//                     'background-color': otc,
//                 }
//             })
//         }
//     }
// }

const disableSelectionMode = (selectionButtonId) => {
    const selectionModeButton = document.getElementById(selectionButtonId);
    current_selection_mode = SELECTION_MODE_DISABLED;
    selectionModeButton.classList.remove('btn-outline-warning');
    selectionModeButton.classList.add('btn-outline-primary');
}

const setSelectionMode = (selectionButtonId, selectionMode) => {
    const selectionModeButton = document.getElementById(selectionButtonId);
    current_selection_mode = selectionMode;
    selectionModeButton.classList.remove('btn-outline-warning');
    selectionModeButton.classList.add('btn-outline-primary');
}

const activateSelectionModeBtn = (btn_id) => {
    const btn = document.getElementById(btn_id);
    btn.classList.remove('btn-outline-primary');
    btn.classList.add('btn-outline-warning');
}
const deactivateSelectionModeBtn = (btn_id) => {
    const btn = document.getElementById(btn_id);
    btn.classList.add('btn-outline-primary');
    btn.classList.remove('btn-outline-warning');
}

const toggleSelectionMode = (selectionMode) => {
    console.log(`toggle selection mode button ${selectionMode}, current selection mode ${current_selection_mode}`)
    if (current_selection_mode === selectionMode) {
        current_selection_mode = SELECTION_MODE_DISABLED;
    } else {
        current_selection_mode = selectionMode;
    }
    deactivateSelectionModeBtn('selectionModeButton');
    deactivateSelectionModeBtn('enhancementSelectionModeButton-right');
    deactivateSelectionModeBtn('enhancementSelectionModeButton-left');
    switch (current_selection_mode) {
        case SELECTION_MODE_FILTER:
            activateSelectionModeBtn('selectionModeButton');
            break;
        case SELECTION_MODE_ENHANCEMENT_RIGHT:
            activateSelectionModeBtn('enhancementSelectionModeButton-right');
            break;
        case SELECTION_MODE_ENHANCEMENT_LEFT:
            activateSelectionModeBtn('enhancementSelectionModeButton-left');
            break;
    }
    // if (!(current_selection_mode === SELECTION_MODE_DISABLED)) {
    //     selectionModeButton.classList.remove('btn-outline-primary');
    //     selectionModeButton.classList.add('btn-outline-warning');
    // } else {
    //     selectionModeButton.classList.remove('btn-outline-warning');
    //     selectionModeButton.classList.add('btn-outline-primary');
    // }
}

const toggleDecisionPoints = () => {
    showDecisionPoints = !showDecisionPoints;
    if (showDecisionPoints) {
        for (const element of graph.nodes()) {
            let data = element.data();
            if (!element.classes().includes('place')) {
                continue;
            }
            const place = data?.place;

            for (let ot in decision_points) {
                console.log(decision_points[ot])
                if (data?.ot === ot && decision_points[ot].includes(place)) {
                    console.log(`Decision Point ${place}`)
                    element.addClass('decision_point');
                }
            }
        }
    } else {
        for (const element of graph.nodes()) {
            element.removeClass('decision_point')
        }
    }
}

const initDecisionBtn = () => {
    const decisionBtn = document.getElementById("toggleDecisionPointsBtn");
    decisionBtn.onclick = (ev) => {
        ev.preventDefault();
        toggleDecisionPoints();
    }
}

initDecisionBtn();
let showDecisionPoints = false;