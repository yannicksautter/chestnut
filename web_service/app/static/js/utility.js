const getQuery = (obj) => {
    return new URLSearchParams(obj).toString();
};
const getAPI = function (input) {
    // TODO assert path not in input
    return `${API_ORIGIN}${input?.path}?${getQuery(input?.params)}`;
}

function guidGenerator() {
    let S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

const show = (element) => {
    element.classList.remove("d-none");
}

const hide = (element) => {
    element.classList.add("d-none");
}

const empty = (element) => {
    const text_inputs = element.querySelectorAll("input[type=text]");
    for (const el of text_inputs) {
        console.log(el);
        el.value = '';
    }
}

const fileFromPath = (path) => {
    return path.replace(/^.*[\\\/]/, '');
}
const getLoadIcon = () => {
    const loadIcon = document.createElement("i");
    loadIcon.setAttribute("class", "fas fa-circle-notch fa-spin");
    loadIcon.setAttribute('id', 'loadIcon');
    loadIcon.style.marginLeft = "0.5em";
    return loadIcon;
};

const removeLoadIcon = () => {
    const loadIcon = document.getElementById('loadIcon');
    if (!loadIcon) {
        return;
    }
    loadIcon.remove();
}



var stringToColour = function(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

const showToastMessage = (header, subtitle, msg) => {
    const getNewToastMessage = () => {
        const defaultToast = document.getElementById("defaultLiveToast");
        const newToast = defaultToast.cloneNode(true);
        return newToast;
    };
    const toastElementContainer = document.getElementById(
        "toastMessageContainer"
    );
    const toastElement = getNewToastMessage();
    const mainTitleElement = toastElement.querySelector(".toast-main-title");
    const subtitleElement = toastElement.querySelector(".toast-subtitle");
    const bodyElement = toastElement.querySelector(".toast-body");

    mainTitleElement.innerText = header;
    subtitleElement.innerText = subtitle;
    bodyElement.innerText = msg;

    toastElementContainer.appendChild(toastElement);

    const toast = new bootstrap.Toast(toastElement);
    console.trace(`Toast Message: ${header}; ${subtitle}; ${msg}`);
    toast.show();
};

const initTooltips = (element = document) => {
    let tooltipTriggerList = [].slice.call(
        element.querySelectorAll('[data-bs-toggle="tooltip"]')
    );
    return tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl);
    });
};
initTooltips()
