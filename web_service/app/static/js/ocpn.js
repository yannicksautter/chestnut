const displayImage = function(ocpn_img_url) {
    const OCPNImage = document.createElement('img');
    // OCPNImage.setAttribute("");
    OCPNImage.style.width = "100%";
    OCPNImage.src = ocpn_img_url;
    return OCPNImage;
}
const showSelectedOCEL = function(ocpn_response) {
    const selectedOCELWrapper = document.getElementById("selectedOCELWrapper");
    show(selectedOCELWrapper);
    // selectedOCELWrapper.classList.remove("d-none");

    const selectedOCELName = document.getElementById("selectedOCELName");
    selectedOCELName.innerText = ocpn_response?.name;

    const selectedOCELFile = document.getElementById("selectedOCELFile");
    selectedOCELFile.innerText = fileFromPath(ocpn_response?.ocel_file);

    const selectedAlgorithm = document.getElementById("selectedAlgorithm");
    selectedAlgorithm.innerText = fileFromPath(ocpn_response?.algorithm);


}

const showFilterOCEL = function (ocpn_response) {
    console.log("Show Filter OCEL")
    const filterWrapper = document.getElementById("filterWrapper");
    show(filterWrapper);
    empty(filterWrapper);
}


const showOCPNModel = function(ocpn_response) {
    const OCPNModelWrapper = document.getElementById("OCPNModelWrapper");
    OCPNModelWrapper.innerHTML = '';

    const displayImageCheck = document.getElementById('displayImage');
    if (displayImageCheck.checked) {
        const OCPNImage = displayImage(ocpn_response?.img)
        OCPNModelWrapper.appendChild(OCPNImage);
    } else {
        show(OCPNModelWrapper);
        const OCPNGraph = displayGraph(ocpn_response?.graph);
        OCPNModelWrapper.appendChild(OCPNGraph);
        showEnhancementContainer();
        // if (ocpn_response?.enhancement == null) {
        //     showEnhancementDataButton();
        // }

    }
    // enhancement = ocpn_response.enhancement;
}

const hideOCPNModal = function () {
    const loadOCELModal = document.getElementById("loadOCELModal");
    const modal = bootstrap.Modal.getOrCreateInstance(loadOCELModal);
    modal.hide();
}



const handleOCPNResponse = function (response) {
    original_frequencies = {
        object: response?.activity_object_frequencies,
        event: response?.activity_event_frequencies
    }
    current_ocpn_id = response?.id;
    current_filtered_ocel = null;
    toggleFilterRemoveButton(false);
    original_object_attributes = response?.object_attributes;
    object_attributes = original_object_attributes
    decision_points = response?.decision_points

    if (response?.duplicate_transitions) {
        showToastMessage("Warning", "","The OCPN contains duplicate transitions." );
    }

    activityFilter = [];
    const selectionModeButton = document.getElementById('selectionModeButton');
    selectionModeButton.classList.remove('btn-outline-warning');
    selectionModeButton.classList.add('btn-outline-primary');
    current_selection_mode = SELECTION_MODE_DISABLED;

    removeLoadIcon();
    hide(document.getElementById('load-wrapper'));
    show(document.getElementById('ocpn_content'));
    hideOCPNModal();
    showOCPNModel(response);
    showSelectedOCEL(response);
    showFilterOCEL(response);
    show(document.getElementById('decisionRow'));


}

const uploadOCPN = function () {
    const formData = new FormData();
    const fileField = document.querySelector('input[type="file"]');
    const ocel_name = document.getElementById("ocel_name").value;
    const ocel_desc = document.getElementById("ocel_desc").value;
    const ocel_algorithm = document.getElementById("DiscoveryAlgorithm").value;

    let parameters = new Map();
    const noiseThreshold = document.getElementById("noiseThreshold")?.value;
    const dependencyThreshold = document.getElementById("dependencyThreshold")?.value;
    parameters.set("noiseThreshold", noiseThreshold);
    parameters.set("dependencyThreshold", dependencyThreshold)

    formData.append('ocel_file', fileField.files[0]);
    formData.append('name', ocel_name);
    formData.append('description', ocel_desc);
    formData.append('algorithm', ocel_algorithm);
    formData.append('parameters',parameters);

    fetch(getAPI({
        path: 'ocpn/',
    }), {
        method: 'POST',
        body: formData
    })
        .then((response) => response.json())
        .then((result) => {
            console.log('Success:', result);
            handleOCPNResponse(result);
            table.ajax.reload();
        })
        .catch((error) => {
            console.error('Error:', error);
            showToastMessage("API Error", "", error)
        });
}
const getOCPN = function (ocpn_id) {
    fetch(getAPI({
        path: `ocpn/${ocpn_id}`,
    }), {
        method: 'GET'
    })
        .then((response) => response.json())
        .then((result) => {
            console.log('Success:', result);
            handleOCPNResponse(result);
        })
        .catch((error) => {
            console.error('Error:', error);
            showToastMessage("API Error", "", error)
        });
}

const initStartBtn = function () {
    const startBtn = document.getElementById('OCPNDiscoveryBtn');

    const loadIcon = getLoadIcon();
    startBtn.onclick = (ev) => {
        ev.preventDefault();
        startBtn.after(loadIcon);
        // Check if here is a select element
        const selectedRows = table.rows({selected: true});
        if (selectedRows.count() > 0) {
            const ocpn_id = table.rows({selected: true}).data()[0].id;
            current_ocpn_id = ocpn_id;
            getOCPN(ocpn_id);
        } else {
            uploadOCPN();
        }
    }
}
const initBrowseInput = function () {
    const browseInput = document.getElementById('id_ocel_file');
    browseInput.onchange = () => {
        table.rows().deselect();
    }
}

initStartBtn();
initBrowseInput();
let current_ocpn_id = null;
let current_filtered_ocel = null;
let current_selection_mode = SELECTION_MODE_DISABLED;
let enhancement = null;
let object_attributes = null;
let original_object_attributes = null;
let original_frequencies = null;
let decision_points = null;
