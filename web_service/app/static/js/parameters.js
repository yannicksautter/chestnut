update_parameters = function() {
     document.getElementById("DiscoveryAlgorithm").onchange = (ev) => {
        show_parameters();
     }
}
show_parameters = function() {
    const ocel_algorithm = document.getElementById("DiscoveryAlgorithm").value;
     const parameters = document.getElementById("parameters");

    if (ocel_algorithm == "inductive miner classic"
     || ocel_algorithm == "inductive miner infrequent"
     || ocel_algorithm == "inductive miner directly follows" ) {
        show(parameters);
        show_inductive_miner_parameters();
    } else if(ocel_algorithm == "heuristic miner classic"
       || ocel_algorithm == "heuristic miner plus plus") {
       show(parameters);
       show_heuristic_miner_parameters();
    } else {
        hide(parameters);
    }
}

show_inductive_miner_parameters = function() {
    const parameters = document.getElementById("parameters");
    parameters.innerText = ''
    parameters.innerHTML= ` <div class="row mt-3">
                                <label for="noiseThreshold" class="col-auto col-form-label">Noise Threshold:  </label>
                                <div class="col-1">
                                    <input type="text" class="form-control" id="noiseThreshold"">
                                </div>
                            </div> `
}

show_heuristic_miner_parameters = function() {
    const parameters = document.getElementById("parameters");
    parameters.innerText = ''
    parameters.innerHTML= ` <div class="row mt-3">
                                <label for="dependencyThreshold" class="col-auto col-form-label">Dependency Threshold:  </label>
                                <div class="col-1">
                                    <input type="text" class="form-control" id="dependencyThreshold"> 
                                </div>
                            </div> `
}

show_parameters();
update_parameters();
