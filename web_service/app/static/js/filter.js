let activityFilter = []
const toggleSelected = (node) => {
    let label = node.data().label;
    const filterActivitiesFormInput = document.getElementById("filterActivities");
    let transition = getTransitionFromLabel(label);
    toggleElement(`"${transition}"`, activityFilter)
    filterActivitiesFormInput.value = activityFilter.join(',');
    node.toggleClass('selected');
}
const getTransitionFromLabel = (label) => {
    return label.replace(/ \(EF:.*|OF:.*\)/g, "")
}
const toggleElement = (element, array) => {
  const index = array.indexOf(element);
  if (index !== -1) {
    // Element is present in the array, so remove it
    array.splice(index, 1);
  } else {
    // Element is not present in the array, so add it
    array.push(element);
  }
}


const displayActivityFrequencies = (activity_event_frequencies, activity_object_frequencies) => {
    for (const element of graph.nodes()) {
        let label = element.data()?.label;
        if (!element.classes().includes('activity')) {
            continue;
        }
        let transition = getTransitionFromLabel(label);
        if (transition == null) {
            element.data("label", transition);
            element.addClass('fade');
            continue;
        }
        if (!activity_event_frequencies.hasOwnProperty(transition)) {
            element.data("label", transition);
            element.addClass('fade');
            continue;
        }
        element.data("label", `${transition} (EF: ${activity_event_frequencies[transition]} | OF: ${activity_object_frequencies[transition]})`);
        element.removeClass('fade')

    }
}

const handleFilterResponse = (result) => {

    const activity_event_frequencies = result?.activity_event_frequencies;
    const activity_object_frequencies = result?.activity_object_frequencies;
    displayActivityFrequencies(activity_event_frequencies, activity_object_frequencies)

    current_filtered_ocel = result?.id;
    object_attributes = result?.object_attributes;

    toggleFilterRemoveButton(true);

}
const toggleFilterRemoveButton = (filter_applied) => {
  const filterApplyButton = document.getElementById('removeFilterButton');
  if (filter_applied) {
    show(filterApplyButton);
  } else {
    hide(filterApplyButton);
  }
};

const initFilterApplyButton = () => {
    const filterApplyButton = document.getElementById('applyFilterButton');
    const loadIcon = getLoadIcon();

    filterApplyButton.onclick = (ev) => {
        ev.preventDefault();
        filterApplyButton.after(loadIcon);
        let data = {};
        let filterActivities = document.getElementById('filterActivities').value;
        let fitness_threshold = document.getElementById('filterFitnessThreshold').value;
        let replay_method = document.getElementById('replayTypeSelect').value
        let must_use_all_activities = document.getElementById('mustUseAllCheckbox').checked;
        let activities = filterActivities.replace(/"+/g,'').split(',');
        let timeFrom = document.getElementById('timeFrom').value;
        let timeTo = document.getElementById('timeTo').value;


        if (filterActivities !== "") {
            data["activities"] = activities;
            data["must_use_all_activities"] = must_use_all_activities
        }
        if (fitness_threshold !== "") {
            data["fitness_threshold"] = fitness_threshold;
            data["replay_method"] = replay_method
        }

        if (timeFrom !== "" && timeTo !== "") {
            data["timeFrom"] = timeFrom
            data["timeTo"] = timeTo
        }
        if (Object.keys(data).length === 0) {
            showToastMessage("Validation Error", "", "Fill out at least one filter option")
            removeLoadIcon();
            return
        }
        fetch(getAPI({
            path: `ocpn/${current_ocpn_id}/filtered_ocels/`
        }), {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then((response) => response.json())
        .then((result) => {
            console.log('Success:', result);
            handleFilterResponse(result);
            disableSelectionMode('selectionModeButton');
            removeLoadIcon();
            showEnhancementContainer();
        })
        .catch((error) => {
            console.error('Error:', error);
            showToastMessage("API Error", "", error)
            removeLoadIcon();
        });
    }
}


const initSelectionModeButton = () => {
    const selectionModeButton = document.getElementById('selectionModeButton');
    selectionModeButton.onclick = (ev) => {
        ev.preventDefault();
        toggleSelectionMode(SELECTION_MODE_FILTER);
    }
}

const initRemoveFilterButton = () => {
    const filterRemoveButton = document.getElementById('removeFilterButton');
    filterRemoveButton.onclick = (ev) => {
        ev.preventDefault();
        current_filtered_ocel = null;
        toggleFilterRemoveButton(false);
        object_attributes = original_object_attributes;
        displayActivityFrequencies(original_frequencies?.event, original_frequencies?.object)
        showEnhancementContainer();
    }
}

initSelectionModeButton();
initFilterApplyButton();
initRemoveFilterButton();