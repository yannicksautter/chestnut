# Generated by Django 4.1.4 on 2022-12-19 15:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0006_filteredocel_annotated_graph"),
    ]

    operations = [
        migrations.RenameField(
            model_name="filteredocel",
            old_name="activity_frequencies",
            new_name="activity_event_frequencies",
        ),
        migrations.RenameField(
            model_name="filteredocel",
            old_name="annotated_graph",
            new_name="activity_object_frequencies",
        ),
    ]
