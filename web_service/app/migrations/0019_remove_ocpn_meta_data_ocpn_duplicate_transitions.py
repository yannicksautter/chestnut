# Generated by Django 4.1.4 on 2023-01-10 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_ocpn_meta_data'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ocpn',
            name='meta_data',
        ),
        migrations.AddField(
            model_name='ocpn',
            name='duplicate_transitions',
            field=models.BooleanField(default=False),
        ),
    ]
