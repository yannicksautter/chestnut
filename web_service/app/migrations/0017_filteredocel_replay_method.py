# Generated by Django 4.1.4 on 2023-01-07 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0016_filteredocel_must_use_all_activities"),
    ]

    operations = [
        migrations.AddField(
            model_name="filteredocel",
            name="replay_method",
            field=models.CharField(max_length=64, null=True),
        ),
    ]
