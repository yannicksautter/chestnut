import pm4py

from django.test import TestCase
from pprint import pprint
from app.discovery import discover_ocpn
from app.enhancement import Enhancement, create_object_level_df
from app.models import (
    ocpn_serialize,
    ocpn_deserialize,
    write_ocel, get_attributes,
)
from app.models import pm4py_ocpn_model_to_json, json_model_to_pm4py_model
from pm4py.visualization.ocel.ocpn import visualizer as ocpn_visualizer


class OCPNTestCase(TestCase):
    """
    All of these test are only to ensure that the functionality does not assert any exceptions.
    """
    def setUp(self):
        pass

    def test_pm4py_ocpn_to_graph(self):
        """Test graph creation"""
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        ocpn = pm4py.discover_oc_petri_net(ocel)
        pm4py.save_vis_ocpn(ocpn, "/tmp/ocpn.png")

    def test_pm4py_ocpn_to_json(self):
        """Testing pm4py ocpn model to json conversion"""
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = discover_ocpn(ocel)
        json_result = pm4py_ocpn_model_to_json(model)
        pprint(json_result)

    def test_pm4py_ocpn_to_json_only_petri_net(self):
        """Testing pm4py ocpn model to json conversion"""
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = discover_ocpn(ocel)
        json_result = pm4py_ocpn_model_to_json(model["petri_nets"]["delivery"][0].arcs)
        pprint(json_result)

    def test_pm4py_ocpn_model_to_gviz(self):
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = discover_ocpn(ocel)
        gviz = ocpn_visualizer.apply(
            model,
            parameters={
                "format": "json",
                "bgcolor": "white",
                "start": "initial_seed",
            },
        )
        self.assertTrue(True)

    def test_json_to_pm4py_ocpn(self):
        """Testing json to pm4py ocpn model conversion"""
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = discover_ocpn(ocel)
        json_result = pm4py_ocpn_model_to_json(model)
        reconstructed_model = json_model_to_pm4py_model(json_result)
        self.assertTrue(True)

    def test_decoding_ocpn_model(self):
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = discover_ocpn(ocel)
        encoded_model = ocpn_serialize(model)
        decoded_model = ocpn_deserialize(encoded_model)
        self.maxDiff = None
        # self.assertEqual(model, decoded_model)
        enhancement = Enhancement(ocel, decoded_model)
        enhancement.apply_fitness_filter(0.5)
        # TODO add check
        # self.assertEqual(model, decoded_model)
        self.assertTrue(True)

    def test_ocpn_model_to_json(self):
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = discover_ocpn(ocel)
        self.assertTrue(True)

    def test_write_ocel(self):
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        write_ocel(ocel)
        self.assertTrue(True)

    def test_get_object_with_attributes(self):
        """
        Test retrieving of object attributes
        """
        ocel = pm4py.read_ocel("samples/pm4py-example_log.jsonocel")
        model = pm4py.discover_oc_petri_net(ocel)

        object_attributes = {}
        for object_type in model["object_types"]:
            object_level_df = create_object_level_df(ocel, object_type)
            attributes = get_attributes(object_level_df)
            if len(attributes) != 0:
                object_attributes[object_type] = get_attributes(object_level_df)
        self.assertTrue(True)

